<%-- 
    Document   : gestionVendedores
    Created on : 09-25-2018, 07:15:37 PM
    Author     : josue
--%>

<%@page import="java.util.*"%>
<%@page import="com.modelo.Vendedor"%>
<%@page import="com.modelo.DaoVendedor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="header.jsp" />
        
        
        <script src="frontend/js/gestionVendedores.js" type="text/javascript"></script>
        <%
            HttpSession sesion=request.getSession();
            if(sesion.getAttribute("nivel")==null)
            {
                response.sendRedirect("index.jsp");
            }
            else
            {
                String nivelU=sesion.getAttribute("nivel").toString();
                String usuario=sesion.getAttribute("nombre").toString();
                
            }
        %>
    </head>
    <body>
        
        <div class="ui  inverted segment" style="background-color:  #008080; margin-top: 0px;">
                    <div class="ui fixed inverted secondary menu" style="background-color: #008080; height: 55px;">
                      <h2 class="item" >
                           GoBilling
                      </h2>
                      
                          <a class="item" style="margin-left: 65%;">
                            <%=sesion.getAttribute("nombre") %>
                            &nbsp;<i class="user circle icon"></i>
                          </a>
                          <a class="item">
                              Cerrar Sesion &nbsp;&nbsp;
                              <i class="ui sign-out icon"></i>
                          </a>
                            
                          
                    </div>
        </div>
        <div class="ui grid">
            
        <div class="ui left fixed vertical inverted menu" style=" margin-top:55px; " >
                        <br><br>
                        <div class="ui vertical labeled icon menu" style="margin-left: 70px;">
                            <a class="item">
                              <i class="massive teal cube icon"></i>
                              Games
                            </a>
                        </div>
                        <br><br><br>
                      <a class="item" href="dashboard.jsp">
                        HOME
                      </a> 
                      <a class="item" href="producto.jsp">
                        Gestionar Productos
                      </a>
                      <a class="item" href="cliente.jsp">
                        Gestionar Clientes
                      </a>
                      <a class="item" href="facturacion.jsp">
                        Facturar
                      </a>
                      <a class="item" href="dashboardReportes.jsp">
                        Reportes
                      </a>
                      <a class="item" href="gestionarUsuarios.jsp">
                        Gestionar Usuarios
                      </a>
                        
         </div>
                    <div class="pusher">
                      
                    </div>
                  </body>
    
        
   <div style=" width: 1100px; height: 558px; margin-left: 250px;">
       <br><br><br>
       <h2 class="ui dividing header ui center aligned grid">Gestionar Vendedores</h2><br><br>
       <input type="button" class="ui green button" style=" margin-left:82%; margin-top: -25%; " id="abrirAgregar" value="NUEVO USUARIO">
       <center>
                            <br>  
                            <div style="margin-left: 20px; margin-right: 20px;">
                                <table class="ui celled table" id="tableV" name="tableV" style="width: 100%;"> 
                                <thead>
                                    <th>Dui</th>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Direccion</th>
                                    <th>Telefono Oficina</th>
                                    <th>Telefono Movil</th>
                                    <th> Acciones </th>
                            </thead>
                                <tbody>
                                    <%
                                        DaoVendedor daoV = new DaoVendedor();
                                        List<Vendedor> vendedores  = daoV.mostrarVendedores();

                                       for(Vendedor v:vendedores)
                                       {
                                    %>
                                        <tr>
                                            <td><%= v.getDui() %></td> 
                                            <td><%= v.getNombre() %></td> 
                                            <td><%= v.getApellidos() %></td> 
                                            <td><%= v.getDireccion() %></td> 
                                            <td><%= v.getTelefonoOficina() %></td> 
                                            <td><%= v.getTelefonoMovil()%></td> 
                                            <td>
                                                <input type="button" name="<%= v.getCodigoVendedor() %>" id="<%= v.getCodigoVendedor() %>" class="ui button modificar" value="Modificar">
                                                <input type="button" name="<%= v.getCodigoVendedor() %>" id="<%= v.getCodigoVendedor() %>" class="ui button eliminar" value="Eliminar">
                                            </td> 
                                        </tr>

                                    <%
                                        }
                                    %>
                                    </tbody>
                                </table>
                            </div>
                        </center>
  </div>
        
        
    </div>            
                
    </body>
    
    <div class="ui modal" id="modalRegistrar">
           <i class="close icon"></i>
           <div class="header">
               Nuevo Cliente
           </div>
           <div class="image content">
               <div class="ui medium image">
                   <img src="frontend/img/add_user.png"/>
               </div>
               <div class="description">
                   <div class="ui header">Datos del Cliente.</div>
                   
                   <form id="formularioIngresarVendedores">
                    <table class="mdl-data-table" >
                            <tr>
                                <td>
                                    <label>Dui</label>
                                </td>
                                <td>
                                    <input type="text" name="txtDui" id="txtDui" required >
                                </td>
                            </tr>  
                            <tr>
                                <td>
                                    <label>Nombre</label>
                                </td>
                                <td>
                                    <input type="text" name="txtNombre" id="txtNombre" required>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Apellido</label>
                                </td>
                                <td>
                                    <input type="text" name="txtApellidos" id=txtApellidos" required>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Direccion</label>
                                </td>
                                <td>
                                    <input type="text" name="txtDireccion" id="txtDireccion" required="">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Telefono de Oficina</label>
                                </td>
                                <td>
                                    <input type="text" name="txtTelOficina" id="txtTelOficina" required="">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Telefono de Movil</label>
                                </td>
                                <td>
                                    <input type="text" name="txtTelefonoMovil" id="txtTelefonoMovil" required="true">
                                    <input type="text" id="key"  name="key" hidden="true">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">

                                    <div class="ui fluid large teal submit button" id="btnAgregar">Agregar Vendedor</div>
                                </td>
                            </tr>
                        </table>
                    </form>
                   
               </div>
           </div>
           <div class="actions">
               <div class="ui black deny button">
                   Cancelar
               </div>
              
           </div>
       </div>
        <%
        
        %>
    <div class="ui modal" id="modalModificar">
           <i class="close icon"></i>
           <div class="header">
               Nuevo Cliente
           </div>
           <div class="image content">
               <div class="ui medium image">
                   <img src="frontend/img/add_user.png"/>
               </div>
               <div class="description">
                   <div class="ui header">Datos del Cliente.</div>
                   
                   <form id="formularioModificarVendedores">
                    <table class="mdl-data-table" >
                            <tr>
                                <td>
                                    <label>Dui</label>
                                </td>
                                <td>
                                    <input type="text" name="txtDuiM" id="txtDuiM" required >
                                </td>
                            </tr>  
                            <tr>
                                <td>
                                    <label>Nombres</label>
                                </td>
                                <td>
                                    <input type="text" name="txtNombreM" id="txtNombreM" required>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Apellidos</label>
                                </td>
                                <td>
                                    <input type="text" name="txtApellidoMod" id="txtApellidoMod" required>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Direccion</label>
                                </td>
                                <td>
                                    <input type="text" name="txtDireccionM" id="txtDireccionM" required="">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Telefono de Oficina</label>
                                </td>
                                <td>
                                    <input type="text" name="txtTelOficinaM" id="txtTelOficinaM" required="">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Telefono de Movil</label>
                                </td>
                                <td>
                                    <input type="text" name="txtTelefonoMovilM" id="txtTelefonoMovilM" required="true">
                                    
                                </td>
                                <input type="text" id="pruebaid"  name="pruebaid" style="display: none;">
                            </tr>
                            <tr>
                                <td colspan="2">

                                    <div class="ui fluid large teal submit button" id="btnModificarM">Guardar Cambios</div>
                                </td>
                            </tr>
                        </table>
                    </form>
                   
               </div>
           </div>
           <div class="actions">
               <div class="ui black deny button">
                   Cancelar
               </div>
              
           </div>
       </div>
      
</html>
