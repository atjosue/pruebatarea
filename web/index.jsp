<%-- 
    Document   : newjspindex.jsp
    Created on : 09-23-2018, 01:09:53 PM
    Author     : Carlos_Campos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <jsp:include page="header.jsp" />
        <title>Acceso</title>
    </head>
    <style>
        body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
    </style>
    <body>
       
        <div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui image header">
      <div class="content">
        Log-in to your account
      </div>
    </h2>
      <form method="post" class="ui large form" id="frmLogin">
      <div class="ui stacked secondary  segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="nombre" placeholder="Nombre de Usuario">
            <input type="hidden" name="key" id="key">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="clave" placeholder="Conraseña">
          </div>
        </div>
          <div class="ui fluid large teal submit button" id="btnStart">Login</div>
      </div>

          <div class="ui negative message" id="respuesta" ></div>

    </form>

    
  </div>
</div>
       
    </body>
</html>
