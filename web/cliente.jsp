<%-- 
    Document   : cliente
    Created on : 09-30-2018, 12:29:17 PM
    Author     : Fernando
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.modelo.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cliente</title>
         <jsp:include page="header.jsp" />
         <script src="frontend/js/cliente.js" type="text/javascript"></script>
         <script src="frontend/js/modificar.js" type="text/javascript"></script> 
        <%
            HttpSession sesion=request.getSession();
            if(sesion.getAttribute("nivel")==null)
            {
                response.sendRedirect("index.jsp");
            }
            else
            {
                String nivelU=sesion.getAttribute("nivel").toString();
                String usuario=sesion.getAttribute("nombre").toString();
                
            }
        %> 
    </head>
    
    
    <body>
        <div class="ui  inverted segment" style="background-color:  #008080; margin-top: 0px;">
                    <div class="ui fixed inverted secondary menu" style="background-color: #008080; height: 55px;">
                      <h2 class="item" >
                           GoBilling
                      </h2>
                      
                          <a class="item" style="margin-left: 65%;">
                            <%=sesion.getAttribute("nombre") %>
                            &nbsp;<i class="user circle icon"></i>
                          </a>
                          <a class="item">
                              Cerrar Sesion &nbsp;&nbsp;
                              <i class="ui sign-out icon"></i>
                          </a>
                            
                          
                    </div>
        </div>
        <div class="ui left fixed vertical inverted menu" style=" margin-top:55px; " >
                        <br><br>
                        <div class="ui vertical labeled icon menu" style="margin-left: 70px;">
                            <a class="item">
                              <i class="massive teal cube icon"></i>
                              Games
                            </a>
                        </div>
                        <br><br><br>
                        <a class="item" href="dashboard.jsp">
                        HOME
                      </a>
                        <a class="item" href="gestionVendedores.jsp">
                        Gestionar Vendedores
                      </a>
                      <a class="item" href="producto.jsp">
                        Gestionar Productos
                      </a>
                      <a class="item" href="facturacion.jsp">
                        Facturar
                      </a>
                      <a class="item" href="dashboardReportes.jsp">
                        Reportes
                      </a>
                      <a class="item" href="gestionarUsuarios.jsp">
                        Gestionar Usuarios
                      </a>
                        
         </div>
                    <div class="pusher">
                      
                    </div>
                            
       <!-- ESTE ES EL CONTENEDOR DE LAS TABLAS Y DEMAS INFROMACION-->   
       
       <div style=" width: 1100px; height: 558px; margin-left: 250px;">
           <br><br>
       <h2 class="ui dividing header ui center aligned grid">Clientes Activos</h2>
      <br>
       <div class="ui equal width grid">
            <div class="sixteen wide column"></div>
           <div class="thirteen wide column"></div>
             <div class="column">
       <button class="ui green button" id="nuevo_Cliente">Nuevo</button>
      
             </div>
           
       </div>
       <div class="ui equal width grid">
       <div class="sexteen wide column">  </div>
       </div>
      <div style="margin-left:50px ; margin-right:50px">
         
      <table id="cliente" class="ui celled table" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Direccion</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
        <%
            DaoCliente daoC = new DaoCliente();
            List<Cliente> cliente = daoC.mostrarClientes();
            for(Cliente cl : cliente){
        %>
        <tr>
            <td class="item"><%= cl.getNombre() %></td>
            <td class="item"> <%= cl.getApellidos() %> </td>
            <td class="item"> <%= cl.getDireccion() %> </td>
            <td>
               
                <a href="javascript:cargar(<%=cl.getCodigoCliente()%>,'<%=cl.getNombre()%>','<%=cl.getApellidos()%>','<%=cl.getDireccion()%>')"><div class="ui blue icon button modificarCliente"><i class="edit icon"></i></div></a>
                <div class="ui left icon input" style="width: 5px;">
                    <input type="buttom" class="ui red icon button eliminarCliente" name="<%=cl.getCodigoCliente()%>" id="<%=cl.getCodigoCliente()%>"/>
                    <i class="user times icon" ></i>
                </div>
         
            </td>
        </tr>
        
        <% } %>
        </tbody>
    </table>
          
      </div>
           
           
       </div>                     
                            
        
       <!-- Modal para ingreso de Clientes -->
       
       <div class="ui modal" id="modalInsertar">
           <i class="close icon"></i>
           <div class="header">
               Nuevo Cliente
           </div>
           <div class="image content">
               <div class="ui medium image">
                   <img src="frontend/img/add_user.png"/>
               </div>
               <div class="description">
                   <div class="ui header">Datos del Cliente.</div>
                   
                   <form class="ui form" id="frmCliente">
                        <div class="two fields">
                       <div class="field">
                           <input type="hidden" name="key" value="insertar"/>
                           <input type="hidden"  name="txtCodigo" value=""/>
                           <label>Nombre</label>
                           <input type="text" name="txtNombre" placeholder="Nombre">
                       </div>
                       <div class="field">
                           <label>Apellido</label>
                           <input type="text"  name="txtApellido" placeholder="Apellido">
                       </div>
                        </div>
                        <div class="field">
                           <label>Dirección</label>
                           <input type="text"  name="txtDireccion" placeholder="Dirección">
                       </div>
                       <buttom class="ui positive right labeled icon button" name="btnGuardar" id="btnGuardar">Guardar<i class="user plus icon"></i></buttom>                       
                       <div class="ui error message"></div>
                   </form>
                   
               </div>
           </div>
           <div class="actions">
               <div class="ui black deny button">
                   Cancelar
               </div>
              
           </div>
       </div>
       
        <!-- Fin Modal para ingreso de Clientes -->
        
        
        <!-- Modal para Modificar Clientes -->
       
       <div class="ui modal" id="modalModificar">
           <i class="close icon"></i>
           <div class="header">
               Modificar datos Cliente
           </div>
           <div class="image content">
               <div class="ui medium image">
                   <img src="frontend/img/add_user.png"/>
               </div>
               <div class="description">
                   <div class="ui header">Datos del Cliente.</div>
                   
                   <form class="ui form" name="frmModificar" id="frmModificar">
                        <div class="two fields">
                       <div class="field">
                           <input type="hidden" name="key" value="modificar"/>
                           <input type="hidden" id="codigo" name="txtCodigoM"/>
                           <label>Nombre</label>
                           <input type="text" id="nombre" name="txtNombreM" placeholder="Nombre">
                       </div>
                       <div class="field">
                           <label>Apellido</label>
                           <input type="text" id="apellido" name="txtApellidoM" placeholder="Apellido">
                       </div>
                        </div>
                        <div class="field">
                           <label>Dirección</label>
                           <input type="text" id="direccion" name="txtDireccionM" placeholder="Dirección">
                       </div>
                       <div class="ui positive right labeled icon button" name="btnModificar" id="btnModificar">Guardar Cambios<i class="edit icon"></i></div>                       
                       <div class="ui error message"></div>
                   </form>
                   
               </div>
           </div>
           <div class="actions">
               <div class="ui black deny button">
                   Cancelar
               </div>
                
               
           </div>
       </div>
       
        <!-- Fin Modal para modificar Clientes -->
        
    </body>
    <script Languaje='JavaScript'>
        function cargar(codigo, nombre, apellido ,direccion)
        {
            $("#codigo").val(codigo);
            $("#nombre").val(nombre);
            $("#apellido").val(apellido);
            $("#direccion").val(direccion);
            console.log(codigo);
            console.log(nombre);
            console.log(apellido);
            console.log(direccion);
        }
    </script>
</html>
