<%-- 
    Document   : facturacion
    Created on : 10-01-2018, 09:54:40 PM
    Author     : Carlos_Campos
--%>

<%@page import="com.modelo.Producto"%>
<%@page import="com.modelo.DaoProducto"%>
<%@page import="com.modelo.DaoFactura"%>
<%@page import="com.modelo.Factura"%>
<%@page import="com.modelo.DetalleFactura"%>
<%@page import="com.modelo.DaoDetalleFactura"%>
<%@page import="com.modelo.Cliente"%>
<%@page import="com.modelo.DaoCliente"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.modelo.Vendedor" %>
<%@page import="com.modelo.DaoVendedor" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="header.jsp" />
        <title>Facturación</title>
        
        <script>
            
    $(document).ready(function(){
        $('#search-select')
      .dropdown()
    ;
    
    $('#search-select2')
      .dropdown()
    ;
    
    $('#selectProductos')
      .dropdown()
    ;
    
    key="action";
     $.ajax({
            type:'post',
            url:'procesarFacturacion',
            data:{key},
            
            success:function (response){
                
               
            
            }

        });
    
    
    
    
    $("#generate").click(function(){
        key="generate";
     $.ajax({
            type:'post',
            url:'procesarFacturacion',
            data:{key},
            
            success:function (response){
                
                var numero="F000"+response;
                $("#numero").val(numero);
            
            }

        });
    });
    
    
    
      $("#detalles").val(1);
      
     
      
        
          var tabla = $('#tblDetalle').DataTable({
       
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    }); // Termina Configuración de DataTable

    // función para ocultar columna de ids
    tabla.column(0).visible(false);

    // Función para centrar acciones de las filas
   

    $(document).on('click', '.page-link', function() {
        $('tr td:last-child').css('display', 'flex');
        $('tr td:last-child').css('justify-content', 'space-around');
        $('tr td:last-child').css('align-items', 'center');
    });
    
      
        
  $("#producto").click(function(){
      
      $('#Detalle')
  .modal('show')
;
   
  });      
       
 $("#facturas").click(function(){
      
      $('#mostrarFacturas')
  .modal('show')
;
   
  });
  
  //cargando datos 
  $("#selectProductos").change(function(){
      
      var idProducto=$("#selectProductos").val();
      console.log(idProducto);
      var key="cargarProducto";
     $.ajax({
            type:'post',
            url:'procesarFacturacion',
            data:{idProducto,key},
            
            success:function (response){
            
                var content=JSON.parse(response);
                
                
                $("#nombreProducto").val(content[0].nombreProducto);
                $("#codigoBarra").val(content[0].codigoBarra);
                $("#precioProducto").val(content[0].precioVenta);
            }

        });
  });
  
  
 
  
  $("#agregarProd").click(function(){
      
     
       
      var numero=$("#numero").val();
       var vendedor=$("#search-select").val();
       var cliente=$("#search-select2").val();
       var fecha=$("#fecha").val();
       var key="insertar";
       var cantidad=$("#cantidad").val();
       var idProducto=$("#selectProductos").val();
       var nombre=$("#nombreProducto").val();
       var precio=$("#precioProducto").val();
       var codigoBarra=$("#codigoBarra").val();
     
     if(numero!=="" && fecha!=="" && cantidad!=="" && idProducto!=="")
     {
      $.ajax({
            type:'post',
            url:'procesarFacturacion',
            data:{numero,vendedor, cliente, fecha,cantidad,idProducto,nombre,precio,codigoBarra,key},
            
            success:function (response){
            
                if(response==="no")
            {
                alert("La Cantidad Es mayor al total de producto disponible");
            }
            else
            {
                location.reload();
            }
            
            
            }

        });
    }
    else
    {
        
        alert("Complete todos los campos");
    }
  });
  
  
  $("#btnFactura").click(function(){
      
     
     var key="nuevaFactura";
     
      $.ajax({
            type:'post',
            url:'procesarFacturacion',
            data:{key},
            
            success:function (response){
            
                location.reload();
            }

        });
      
  });
  
  $(".modificarMod").click(function(){
          
        
        
        var idUsuario = $(this).attr('id');
        var key = "getDetalle";
         $.ajax({
            type:'post',
            url:'procesarFacturacion',
            data:{key,idUsuario},
            
            success:function (response){
            
             var content=JSON.parse(response);
                $("#idDetalle").val(idUsuario);
                $("#cantidadM").val(content[0].cantidad);
                 $("#selectProductosM").val(content[0].codigoProducto);
                $("#nombreProductoM").val(content[0].nombreProducto);
                $("#codigoBarraM").val(content[0].codigoBarra);
                $("#precioProductoM").val(content[0].precioVenta);
                $("#idFacturaM").val(content[0].codigoFactura);
                
                $("#Modificar").modal("show");
            }

        });
        
        
        
                
    });
    
    $("#modificarProd").click(function(){
        
        
        
       var idDetalle = $("#idDetalle").val();
       var key="modificar";
       var cantidad=$("#cantidadM").val();
       var idProducto=$("#selectProductosM").val();
       var nombre=$("#nombreProductoM").val();
       var precio=$("#precioProductoM").val();
       var codigoBarra=$("#codigoBarraM").val();
       var idFactura=$("#idFacturaM").val();
     if(cantidad!=="" && idProducto!=="")
     {
     alert(idFactura);
      $.ajax({
            type:'post',
             url:'procesarFacturacion',
            data:{idDetalle,cantidad,idProducto,nombre,precio,codigoBarra,key,idFactura},
            
            success:function (response){
            if(response===0)
            {
                alert("La Cantidad Es mayor al total de producto disponible");
            }
            else
            {
                location.reload();
            }
        
    }

        });
    }
    else
    {
       alert("Por favor complete todos los Campos"); 
        
    }
        
        
    });
    
    $(".eliminar").click(function(){
          
        
        
        var idUsuario = $(this).attr('id');
        var key = "getDetalle";
         $.ajax({
            type:'post',
            url:'procesarFacturacion',
            data:{key,idUsuario},
            
            success:function (response){
            
             var content=JSON.parse(response);
                var idDetalle=idUsuario;
                var idFactura=content[0].codigoFactura;
                var codProd=content[0].codigoProducto;
                console.log(codProd);
                 
                key="eliminar";
            $.ajax({
            type:'post',
            url:'procesarFacturacion',
            data:{key,idDetalle,idFactura, codProd},
            
            success:function (response){
            
             
                 
                location.reload();
                
            }

        });
            }

        });
        
        
        
                
    });
  
  
  
      
      
  
  //ingresando productos
  
  
  
    });      
    
        </script>
       <% HttpSession sesion=request.getSession();
       
                
           if(sesion.getAttribute("action")==null)
           {
                 sesion.setAttribute("id", "");
                 sesion.setAttribute("numero", "");
                 sesion.setAttribute("cliente", "" );
                 sesion.setAttribute("vendedor", "");
                 sesion.setAttribute("fecha", "");
                
            
           }  
              
       %>
       <%
           DaoProducto daoP= new DaoProducto();
       Factura f= new Factura();%>
    </head>
    <body>
          <div class="ui  inverted segment" style="background-color:  #008080; margin-top: 0px;">
                    <div class="ui fixed inverted secondary menu" style="background-color: #008080; height: 55px;">
                      <h2 class="item" >
                           GoBilling
                      </h2>
                      
                          <a class="item" style="margin-left: 65%;">
                            <%=sesion.getAttribute("nombre") %>
                            &nbsp;<i class="user circle icon"></i>
                          </a>
                          <a class="item">
                              Cerrar Sesion &nbsp;&nbsp;
                              <i class="ui sign-out icon"></i>
                          </a>
                            
                          
                    </div>
        </div>
                            <div class="ui left fixed vertical inverted menu" style=" margin-top:55px; " >
                        <br><br>
                        <div class="ui vertical labeled icon menu" style="margin-left: 70px;">
                            <a class="item">
                              <i class="massive teal cube icon"></i>
                              Games
                            </a>
                        </div>
                        <br><br><br>
                        <a class="item" href="dashboard.jsp">
                        HOME
                      </a>
                        <a class="item" href="gestionVendedores.jsp">
                        Gestionar Vendedores
                      </a>
                      <a class="item" href="producto.jsp">
                        Gestionar Productos
                      </a>
                      <a class="item" href="facturacion.jsp">
                        Facturar
                      </a>
                      <a class="item" href="dashboardReportes.jsp">
                        Reportes
                      </a>
                      <a class="item" href="gestionarUsuarios.jsp">
                        Gestionar Usuarios
                      </a>
                        
         </div>
        <div class="ui grid">
          
                            <div class="pusher">
                      
                    </div>
            <div class="five wide column" ></div>
        <div class="six wide column">
            <h1>Facturas </h1>
            <form class="ui form" id="insertarFactura">
                    
                        <div class="field">
                            <label>Numero Factura </label>
                            
                            <input type="text" name="numero" id="numero" placeholder="Numero" value="<%= sesion.getAttribute("numero").toString()%>" readonly="readonly">
                          <input type="button" name="generate" id="generate"  value="Generar Codigo">
                        </div>
                        <div class="field">
                            <label>Vendedor</label>
                            
                        <select class="ui search selection dropdown" id="search-select" name="vendedor" >
                            <%
                                DaoVendedor daoV= new DaoVendedor();
                                List<Vendedor> lst=daoV.mostrarVendedores();
                                for(Vendedor v:lst)
                                {
                                if(sesion.getAttribute("numero").equals(null) ||sesion.getAttribute("numero").equals("") )
                                {
                                
                            %>
                            <option value="<%=v.getCodigoVendedor() %>"><%=v.getNombre() %> &nbsp; <%=v.getApellidos() %></option>
                        <%
                            }
                            
                            else{
                            %>
                            <option value="<%=sesion.getAttribute("vendedor").toString() %>"><%=v.getNombre() %> &nbsp; <%=v.getApellidos() %></option>
                             <%   }
                                }
                             %>
                        </select>
                      </div>
                        <div class="field">
                            <label>Cliente</label>
                            
                        <select class="ui search selection dropdown" id="search-select2" name="cliente">
                            <%
                                DaoCliente daoC= new DaoCliente();
                                
                                List<Cliente> lstC=daoC.mostrarClientes();
                                for(Cliente c:lstC)
                                {
                            %>
                            <option value="<%=c.getCodigoCliente() %>"><%=c.getNombre() %> &nbsp; <%=c.getApellidos() %></option>
                        <%
                            }
                            %>
                        </select>
                      </div>
                        
                        <div class="field">
                          <label>Fecha Facturacion</label>
                          <input type="date" name="fecha" placeholder="Fecha" id="fecha" value="<%=sesion.getAttribute("fecha").toString() %>">
                        </div>
                    
                      
                        <button class="ui button " type="button" name="btnProducto" id="producto">Agregar Producto</button>
                        <button class="ui button" type="button" name="btnFactura" id="btnFactura">Nueva Factura</button>
                        <button class="ui button" type="button" name="btnNew" id="facturas">Ver Facturas </button>
                      </form>

                     
        </div>
                        
             <div class="twelve wide column" style="margin-left:300px ;" >
             <table id="tblDetalle" class="ui celled table" >
                 <input type="hidden" id="detalles" name="factura">
                 <thead class="">
              <tr>
                  <th class="" >codigoDetalle</th>
                  <th class="" >Factura</th>
                  <th class="" >Codigo Producto</th>
                  <th class="" >Codigo Barras</th>
                  <th class="" >Producto</th>
                  <th class="" >Cantidad</th>
                  <th class="" >Precio Venta</th>
                  <th class="" >SubTotal</th>
                  <th class="" >Acciones</th>
              </tr>
      </thead>
      <tbody>
          <%
                           DetalleFactura df= new DetalleFactura();
                            DaoDetalleFactura daoD= new DaoDetalleFactura();
                            Factura fact= new Factura();
                            
                        if(!sesion.getAttribute("id").equals(""))
                              {     
                  List<DetalleFactura> detalles= daoD.mostrarDetalle(Integer.parseInt(sesion.getAttribute("id").toString()));
                  
                             
                            for(DetalleFactura det: detalles)
                            {
                        %>
                        <tr>
                            
                            <td><%=det.getCodigoDetalle() %></td>
                            <td><%=det.getCodigoFactura() %></td>
                            <td><%=det.getCodigoProducto() %></td>
                            <td><%=det.getCodigoBarra() %></td>
                            <td><%=det.getNombreProducto() %></td>
                            <td><%=det.getPrecioVenta() %></td>
                            <td><%=det.getCantidad() %></td>
                            <td><%=det.getTotal() %></td>
                            <td><button class="ui blue icon button modificarMod" id="<%=det.getCodigoDetalle()%>"><i class="edit icon"></i></button>
                                <button class="ui red icon button eliminar" id="<%=det.getCodigoDetalle()%>"><i class="euser times icon"></i></button></td>
                            
                            
                            
                            
                            
                            
                        </tr>
                        <%
                            }

                        }
                        else
                        {
                          %>
                          <tr>
                            
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            
                            
                            
                            
                            
                            
                        </tr>
                          <%
                        }
                        %>
      </tbody>
                        </table>
                 </div>
      
      
      
                       
      <div class="ui green" style="margin-left: 65%;" >
          <h1>Total:
              <%if(!sesion.getAttribute("id").equals(""))
                              {   %>
              <%=daoD.totalFactura(Integer.parseInt(sesion.getAttribute("id").toString())) %></h1>
              <%
              
}              %>
      </div>
    
            
            
        </div>
                        
<!-- MODAL INSERTAR DETALLE -->
<div class="ui modal" id="Detalle">
  <div class="header">Agregar Producto</div>
  <div class="scrolling content">
      <form class="ui form" id="insertarDetalle">
          <div class="two field">
                        <div class="field two wide column" >
                          <label>Cantidad</label>
                          <input type="number" name="cantidad" placeholder="Cantidad" id="cantidad">
                        </div>
              <div class="field">
                            <label>Producto</label>
                            
                        <select class="ui search selection dropdown" id="selectProductos" name="producto">
                            <%
                                
                                List<Producto> lstP=daoP.mostrarProducto();
                                for(Producto p:lstP)
                                {
                            %>
                            <option value="<%=p.getCodigoProducto() %>"><%=p.getNombreProducto() %></option>
                        <%
                            }
                            %>
                        </select>
                      </div>
              
          </div>
          
          <div class="field two field" >
              <div class="field">
                          <label>Nombre Producto</label>
                          <input type="text" name="nombreProducto" placeholder="Producto"  id="nombreProducto" readonly="readonly" >
              </div>
              <div class="field">
                          <label>Codigo de Barra</label>
                          <input type="text" name="codigoBarra" placeholder="CodigoBarra"  id="codigoBarra" readonly="readonly" >
              </div>
          </div>
         
              <div class="field">
                          <label>Precio</label>
                          <input type="text" name="precioProducto" placeholder="Precio"  id="precioProducto" readonly="readonly" >
                          
              </div>
              
         
     <div class="actions">
    <div class="ui black deny button">
      Cancelar
    </div>
         <div class="ui positive right labeled icon button" id="agregarProd">
      Guardar
      <i class="checkmark icon"></i>
    </div>
  </div>
          
      </form>
    
  </div>
</div>

                        <!-- MODAL Modificar DETALLE -->
<div class="ui modal" id="Modificar">
  <div class="header">Modificar Producto</div>
  <div class="scrolling content">
      <form class="ui form" id="insertarDetalleM">
          <div class="two field">
                        <div class="field two wide column" >
                          <label>Cantidad</label>
                          <input type="number" name="cantidad" placeholder="Cantidad" id="cantidadM">
                          <input type="hidden" id="idDetalle">
                          <input type="hidden" id="idFacturaM" name="idFacturaM">
                        </div>
              <div class="field">
                            <label>Producto</label>
                            
                        <select class="ui search selection dropdown" id="selectProductosM" name="producto">
                            <%
                                
                                List<Producto> lstprod=daoP.mostrarProducto();
                                for(Producto p:lstprod)
                                {
                            %>
                            <option value="<%=p.getCodigoProducto() %>"><%=p.getNombreProducto() %></option>
                        <%
                            }
                            %>
                        </select>
                      </div>
              
          </div>
          
          <div class="field two field" >
              <div class="field">
                          <label>Nombre Producto</label>
                          <input type="text" name="nombreProducto" placeholder="Producto"  id="nombreProductoM" readonly="readonly" >
              </div>
              <div class="field">
                          <label>Codigo de Barra</label>
                          <input type="text" name="codigoBarra" placeholder="CodigoBarra"  id="codigoBarraM" readonly="readonly" >
              </div>
          </div>
         
              <div class="field">
                          <label>Precio</label>
                          <input type="text" name="precioProducto" placeholder="Precio"  id="precioProductoM" readonly="readonly" >
              </div>
              
         
     <div class="actions">
    <div class="ui black deny button">
      Cancelar
    </div>
         <div class="ui positive right labeled icon button" id="modificarProd">
      Guardar
      <i class="checkmark icon"></i>
    </div>
  </div>
          
      </form>
    
  </div>
</div>
<!-- MODAL INSERTAR DETALLE -->
<div class="ui modal" id="mostrarFacturas">
  <div class="header">Facturas</div>
  <div class="scrolling content">
    <table id="tblDetalle" class="ui five column table">
                 <thead class="">
              <tr>
                  <th class="" >Codigo Factura</th>
                  <th class="" >Numero Factura</th>
                  <th class="" >Codigo Vendedor</th>
                  <th class="" >Codig Cliente</th>
                  <th class="" >Total Venta</th>
                  <th class="" >Fecha Registro</th>
                  <th >Acciones</td>
                  
              </tr>
      </thead>
      <tbody>
          <%
                            
                            DaoFactura daoF= new DaoFactura();
                            List<Factura> facturas= daoF.mostrarFacturas();
                            for(Factura fac: facturas)
                            {
                        %>
         <tr>
                            <td><%=fac.getCodigoFactura() %></td>
                            <td ><%=fac.getNumeroFactura() %></td>
                            <td ><%=fac.getCodigoVendedor() %></td>
                            <td ><%=fac.getCodigoCliente() %></td>
                            <td ><%=fac.getTotalVenta() %></td>
                            <td ><%=fac.getFechaRegistro() %></td>
                            
                            
                            <td ><button class="ui green button" id="modificar" value="Modificar">Modificar</button>&nbsp;<button class="ui red button" id="eliminar" value="ELiminar">Eliminar</button></td>
                            
                            
                            
                            
                            
                            
                        </tr>
                        <%
                            }
                        %>
      </tbody>
                        </table>  
    
  </div>
</div>

    </body>
</html>
