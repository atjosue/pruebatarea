<%-- 
    Document   : producto
    Created on : 2/10/2018, 11:45:51 PM
    Author     : Kevin Montiel
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.modelo.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Producto</title>
         <jsp:include page="header.jsp" />
         <script src="frontend/js/producto.js" type="text/javascript"></script>
         
        <%
            HttpSession sesion=request.getSession();
            if(sesion.getAttribute("nivel")==null)
            {
                response.sendRedirect("index.jsp");
            }
            else
            {
                String nivelU=sesion.getAttribute("nivel").toString();
                String usuario=sesion.getAttribute("nombre").toString();
                
            }
        %> 
    </head>
    
    
    <body>
        <div class="ui  inverted segment" style="background-color:  #008080; margin-top: 0px;">
                    <div class="ui fixed inverted secondary menu" style="background-color: #008080; height: 55px;">
                      <h2 class="item" >
                           GoBilling
                      </h2>
                      
                          <a class="item" style="margin-left: 65%;">
                            <%=sesion.getAttribute("nombre") %>
                            &nbsp;<i class="user circle icon"></i>
                          </a>
                          <a class="item">
                              Cerrar Sesion &nbsp;&nbsp;
                              <i class="ui sign-out icon"></i>
                          </a>
                            
                          
                    </div>
        </div>
        
                            <div class="ui left fixed vertical inverted menu" style=" margin-top:55px; " >
                        <br><br>
                        <div class="ui vertical labeled icon menu" style="margin-left: 70px;">
                            <a class="item">
                              <i class="massive teal cube icon"></i>
                              Games
                            </a>
                        </div>
                        <br><br><br>
                        <a class="item" href="dashboard.jsp">
                        HOME
                      </a>
                        <a class="item" href="gestionVendedores.jsp">
                        Gestionar Vendedores
                      </a>
                      <a class="item" href="cliente.jsp">
                        Gestionar Clientes
                      </a>
                      <a class="item" href="facturacion.jsp">
                        Facturar
                      </a>
                      <a class="item" href="dashboardReportes.jsp">
                        Reportes
                      </a>
                      <a class="item" href="gestionarUsuarios.jsp">
                        Gestionar Usuarios
                      </a>
                        
         </div>
                    <div class="pusher">
                      
                    </div>
       
    <!-- ESTE ES EL CONTENEDOR DE LAS TABLAS Y DEMAS INFROMACION-->                        
    <div style=" width: 1100px; height: 558px; margin-left: 250px;">
        <br>
        <br>
       <h2 class="ui dividing header ui center aligned grid">Productos Activos</h2>
      <br>
       <div class="ui equal width grid">
            <div class="sixteen wide column"></div>
           <div class="thirteen wide column"></div>
             <div class="column">
       <button class="ui green button" id="nuevo_Producto">Nuevo</button>
      
             </div>
           
       </div>
       <div class="ui equal width grid">
       <div class="sexteen wide column">  </div>
       </div>
      <div style="margin-left:50px ; margin-right:50px">
         
      <table id="producto" class="ui celled table" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Stock Minimo</th>
                <th>Stock Actual</th>
                <th>Codigo De Barra</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
        <%
            DaoProducto daoP = new DaoProducto();
            List<Producto> producto = daoP.mostrarProducto();
            for(Producto pro : producto){
        %>
        <tr>
            <td class="item"><%= pro.getNombreProducto() %></td>
            <td class="item"> <%= pro.getPrecioVenta() %> </td>
            <td class="item"> <%= pro.getStockMinimo() %> </td>
            <td class="item"> <%= pro.getStockActual()%> </td>
            <td class="item"> <%= pro.getCodigoBarra()%> </td>
            <td>
               
                <a href="javascript:cargar(<%=pro.getCodigoProducto()%>,'<%=pro.getNombreProducto()%>','<%= pro.getPrecioVenta()%>','<%=pro.getStockMinimo()%>','<%=pro.getStockActual()%>','<%=pro.getCodigoBarra()%>')"><div class="ui blue icon button modificarCliente"><i class="edit icon"></i></div></a>
                <div class="ui left icon input" style="width: 5px;">
                    <input type="button" class="ui red icon button eliminarProducto" id="<%=pro.getCodigoProducto()%>" value=""/>
                    <i class="user times icon"></i>
                </div>
         
            </td>
        </tr>
        
        <% } %>
        </tbody>
    </table>
          
      </div>
        
        
        
    </div>
        
        
       <!-- Modal para ingreso de Clientes -->
       
       <div class="ui modal" id="modalInsertar">
           <i class="close icon"></i>
           <div class="header">
               Nuevo Producto
           </div>
           <div class="image content">
               <div class="ui medium image">
                   <img src="frontend/img/add_product.png"/>
               </div>
               <div class="description">
                   <div class="ui header">Datos de Producto:</div>
                   
                   <form class="ui form" id="frmProducto">
                        <div class="two fields">
                       <div class="field">
                           <input type="hidden" name="key" value="insertar"/>
                           <input type="hidden"  name="txtCodigo" value=""/>
                           <input type="hidden"  name="txtEstado" value="1"/>
                           <label>Nombre Producto</label>
                           <input type="text" name="txtNombre" placeholder="Nombre Producto">
                       </div>
                       <div class="field">
                           <label>Precio De Venta</label>
                           <input type="number" step="any" name="txtPrecio" placeholder="Precio">
                       </div>
                        </div>
                        <div class="field">
                           <label>Stock Minimo</label>
                           <input type="number" name="txtStockM" placeholder="Stock Minimo">
                       </div>
                       <div class="field">
                           <label>Stock Actual</label>
                           <input type="number" name="txtStockA" placeholder="Stock Actual">
                       </div>
                       <div class="field">
                           <label>Codigo de Barras</label>
                           <input type="text" name="txtCodigoB" placeholder="Codigo De Barras">
                       </div>
                       <div class="ui positive right labeled icon button" name="btnGuardar" id="btnGuardar">Guardar<i class="user plus icon"></i></div>                       
                       <div class="ui error message"></div>
                   </form>
                   
               </div>
           </div>
           <div class="actions">
               <div class="ui black deny button">
                   Cancelar
               </div>
              
           </div>
       </div>
       
        <!-- Fin Modal para ingreso de Clientes -->
        
        
        <!-- Modal para Modificar Clientes -->
       
       <div class="ui modal" id="modalModificar">
           <i class="close icon"></i>
           <div class="header">
               Modificar datos Producto
           </div>
           <div class="image content">
               <div class="ui medium image">
                   <img src="frontend/img/mod_product.png"/>
               </div>
               <div class="description">
                   <div class="ui header">Datos del Proucto.</div>
                   
                   <form class="ui form" name="frmModificar" id="frmModificar">
                        <div class="two fields">
                       <div class="field">
                           <input type="hidden" name="key" value="modificar"/>
                           <input type="hidden" id="codigo" name="txtCodigoM"/>
                           <label>Nombre Producto</label>
                           <input type="text"  id="nombre"name="txtNombreM" placeholder="Nombre Producto">
                       </div>
                       <div class="field">
                           <label>Precio De Venta</label>
                           <input type="number" step="any"  id="precio"name="txtPrecioM" placeholder="Precio">
                       </div>
                        </div>
                        <div class="field">
                           <label>Stock Minimo</label>
                           <input type="number" id="stockM" name="txtStockMM" placeholder="Stock Minimo">
                       </div>
                       <div class="field">
                           <label>Stock Actual</label>
                           <input type="number" name="txtStockAM" id="stockA" placeholder="Stock Actual">
                       </div>
                       <div class="field">
                           <label>Codigo de Barras</label>
                           <input type="text" name="txtCodigoBM"  id="codigoB"placeholder="Codigo De Barras">
                       </div>
                       <div class="ui positive right labeled icon button" name="btnModificar" id="btnModificar">Guardar Cambios<i class="edit icon"></i></div>                       
                       <div class="ui error message"></div>
                   </form>
                   
               </div>
           </div>
           <div class="actions">
               <div class="ui black deny button">
                   Cancelar
               </div>
                
               
           </div>
       </div>
       
        <!-- Fin Modal para modificar Clientes -->
        
    </body>
    <script Languaje="JavaScript">
        function cargar(codigo, nombre, precio ,stockM, stockA, codigoB)
        {
            $("#codigo").val(codigo);
            $("#nombre").val(nombre);
            $("#precio").val(precio);
            $("#stockM").val(stockM);
            $("#stockA").val(stockA);
            $("#codigoB").val(codigoB);
            
            console.log(codigo);
            console.log(nombre);
            console.log(precio);
            console.log(stockM);
            console.log(stockA);
            console.log(codigoB);
        }
    </script>
</html>

