<%-- 
    Document   : dashboard
    Created on : 09-27-2018, 10:27:41 PM
    Author     : Carlos_Campos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="header.jsp" />
        <title>Administracion</title>
         <%
            HttpSession sesion=request.getSession();
            if(sesion.getAttribute("nivel")==null)
            {
                response.sendRedirect("index.jsp");
            }
            else
            {
                String nivelU=sesion.getAttribute("nivel").toString();
                String usuario=sesion.getAttribute("nombre").toString();
                
            }
        %>
    </head>
        
    <body>
        
            <div class="ui  inverted segment" style="background-color:  #008080; margin-top: 0px;">
                    <div class="ui fixed inverted secondary menu" style="background-color: #008080; height: 55px;">
                      <h2 class="item" >
                           GoBilling
                      </h2>
                      
                          <a class="item" style="margin-left: 65%;">
                            <%=sesion.getAttribute("nombre") %>
                            &nbsp;<i class="user circle icon"></i>
                          </a>
                            <a class="item" href="cerrarSesion.jsp">
                              Cerrar Sesion &nbsp;&nbsp;
                              <i class="ui sign-out icon"></i>
                          </a>
                            
                          
                    </div>
            </div>
                            
    <div class="ui grid">
         <div class="ui left fixed vertical inverted menu" style=" margin-top:55px; " >
                        <br><br>
                        <div class="ui vertical labeled icon menu" style="margin-left: 70px;">
                            <a class="item">
                              <i class="massive teal cube icon"></i>
                              Games
                            </a>
                        </div>
                        <br><br><br>
                        <% if(sesion.getAttribute("nivel").equals(1) || sesion.getAttribute("nivel").equals(3)){%>
                        <a class="item" href="gestionVendedores.jsp">
                        Gestionar Vendedores
                      </a>
                         <%} %>
                         
                      <a class="item" href="producto.jsp">
                        Gestionar Productos
                      </a>
                      <a class="item" href="cliente.jsp">
                        Gestionar Clientes
                      </a>
                         
                      <a class="item" href="facturacion.jsp">
                        Facturar
                      </a>
                      <a class="item" href="dashboardReportes.jsp">
                        Reportes
                      </a>
                        <% if(sesion.getAttribute("nivel").equals(1)){%>
                      <a class="item" href="gestionarUsuarios.jsp">
                        Gestionar Usuarios
                      </a>
                     <%} %>
         </div>
                    <div class="pusher">
                      
                    </div>
                  </body>
    </div>
   
    </body>
        
    </html>
