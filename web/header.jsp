<%-- 
    Document   : header
    Created on : 09-23-2018, 01:10:29 PM
    Author     : Carlos_Campos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="frontend/Semantic-UI-CSS-master/semantic.min.css" rel="stylesheet" type="text/css"/>
        <script src="frontend/js/jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="frontend/Semantic-UI-CSS-master/semantic.min.js" type="text/javascript"></script>
        <script src="frontend/js/script.js" type="text/javascript"></script>
        <script src="frontend/js/ajaxTransactions.js" type="text/javascript"></script>
        <link href="frontend/DataTables/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <script src="frontend/DataTables/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <link href="frontend/sweetalert2.0/sweetalert.css" rel="stylesheet" type="text/css"/>
        <script src="frontend/sweetalert2.0/sweetalert2.js" type="text/javascript"></script>
      
        
        
    </head>
  
</html>
