drop database if exists facturacion;

create database facturacion;
use facturacion;
create table cliente(
	codigoCliente int primary key auto_increment,
    nombre varchar(50),
    apellidos varchar(50),
    direccion varchar(100),
    estado int

);



# ingresando clientes
insert into cliente values(0, 'Josue','Amaya', 'San Salvador',1);
insert into cliente values(0, 'carlos','campos', 'San Salvador',1);
insert into cliente values(0, 'fernando','flamenco', 'San Salvador',1);
insert into cliente values(0, 'kevin','Montiel', 'San Salvador',1);
insert into cliente values(0, 'Carla','Tzec', 'San Salvador',1);
insert into cliente values(0, 'Fernanda','Galdamex', 'San Salvador',1);
insert into cliente values(0, 'Antonia','Pimentel', 'San Salvador',1);

create table vendedor(
	codigoVendedor int primary key auto_increment,
    dui varchar(10),
    nombre varchar(50),
    apellidos varchar(50),
    direccion varchar(100),
    telefonoOficina varchar(30),
    telefonoMovil varchar(30),
	estado int
);
select * from vendedor;
# insertando vendedores
insert into vendedor values(0,'04455445-8','Diego', 'Hernandez', 'La Libertad', '2345-4556', '7349-4567',1);
insert into vendedor values(0,'04455445-7','Jorge', 'Pimentel', 'La Libertad', '2345-4556', '7349-4567',1);
insert into vendedor values(0,'04455445-6','Abra', 'Lamaleta', 'La Libertad', '2345-4556', '7349-4567',1);
insert into vendedor values(0,'04455445-5','Isrra', 'Mendez', 'La Libertad', '2345-4556', '7349-4567',1);
insert into vendedor values(0,'04455445-4','Luisa', 'Rene', 'La Libertad', '2345-4556', '7349-4567',1);
insert into vendedor values(0,'04455445-3','Carla', 'Mercedez', 'La Libertad', '2345-4556', '7349-4567',1);

create table factura(
	codigoFactura int primary key auto_increment,
    numeroFactura varchar(10),
    codigoVendedor int,
    codigoCliente int,
    totalVenta double,
	fechaRegistro timestamp,
    
    constraint fk_factura_vendedor foreign key(codigoVendedor) references vendedor(codigoVendedor),
    constraint fk_factura_cliente foreign key(codigoCliente) references cliente(codigoCliente)
);

insert into factura values(0, 'F001', 1, 1, '0.0','2018-10-21');


create table producto(
	codigoProducto int primary key auto_increment,
    nombreProducto varchar(50),
	precioVenta double,
    stockMinimo int,
    stockActual int,
    codigoBarra varchar(50),
    estado int
);

insert into producto values(0,'Pasta Dental',28.95, 5, 19,'0003456', 1 );
insert into producto values(0,'Pelota Plastica',28.95, 5, 19,'0003456',1  );
insert into producto values(0,'Producto 3',285, 5, 19,'0003456',1  );
insert into producto values(0,'Pruducto 4',2.95, 5, 19,'0003456',1  );
insert into producto values(0,'Producto 5',25, 5, 19,'0003456',1  );
insert into producto values(0,'Producto 6',28, 5, 19,'0003456',1  );
insert into producto values(0,'Producto 7',95, 5, 19,'0003456',1  );
insert into producto values(0,'Producto 8',295, 5, 19,'0003456',1  );



create table detallefactura(
	codigoDetalle int primary key auto_increment,
    codigoFactura int,
    codigoProducto int,
    codigoBarra varchar(30),
    nombreProducto varchar(50),
    cantidad int,
    precioVenta Double,
    total Double,
    
    constraint fk_detalleFactura_factura foreign key(codigoFactura) references factura(codigoFactura)
);

insert into detallefactura values(0,1,2,'0003456','Pelota Plastica',3,28.95,144.75);
insert into detallefactura values(0,1,3,'0003456','Producto 3',20,28.95,144.75);
insert into detallefactura values(0,1,4,'0003456','Producto 4',8,28.95,144.75);
insert into detallefactura values(0,1,3,'0003456','Producto 3',5,28.95,144.75);
insert into detallefactura values(0,1,4,'0003456','Producto 4',1,28.95,144.75);
insert into detallefactura values(0,1,4,'0003456','Producto 4',8,28.95,144.75);

SELECT nombreProducto as nombre, cantidad as cantidad FROM detallefactura ORDER BY cantidad desc;

SELECT d.nombreProducto , SUM(d.cantidad) AS cantidad
FROM detallefactura d
GROUP BY d.nombreProducto
ORDER BY SUM(d.cantidad) DESC
LIMIT 0 , 3;




#agregando detalle 

insert into detallefactura values(0, 1,1,'0003456','Pasta Dental', 5,28.95,144.75);

create table rol(
codigoRol int not null auto_increment primary key,
nombreRol varchar(50)
);

create table usuario(
codigoUsuario int not null auto_increment primary key,
nombre varchar(100),
nombreUsuario varchar(50),
clave varchar(150),
codigoRol int,
 constraint fk_usuarioRol foreign key(codigoRol) references rol(codigoRol)
);

# insersion de datos a tabla de rol
insert into rol values(0,'Administrador');
insert into rol values(0,'Vendedor');

select * from producto;

insert into usuario values(0,'Carlos Eduardo Campos', 'CarlosCampos', '6116afedcb0bc31083935c1c262ff4c9',1);
insert into usuario values(0,'Josue Alexander Amaya', 'JosueAmaya', '6116afedcb0bc31083935c1c262ff4c9',1);
insert into usuario values(0,'Josue Alexander Amaya', 'EduardoCampos', '6116afedcb0bc31083935c1c262ff4c9',2);
select codigoRol from usuario where nombreUsuario='CarlosCampos' and clave='6116afedcb0bc31083935c1c262ff4c9'


delimiter $ 
create procedure addDetalle(in codFactura int,in codProducto int, in codBarra varchar(50), in producto varchar(50),in cantidad int, in precioVenta double, in total double)
begin 
insert into detallefactura values(0,codFactura,codProducto,codBarra, producto,cantidad, precioVenta, total);
update producto set stockActual=stockActual-cantidad where codigoProducto=codProducto;
update factura set totalVenta=totalVenta+total where codigoFactura=codFactura;
end$

delimiter $
create procedure addFactura(in numFactura varchar(15),in codVendedor  int, in codCliente int, in total double,in fecha date)
begin 
insert into factura values(0,numFactura,codVendedor,codCliente, total,fecha);

end$

delimiter $
create procedure updateDetalle(in codFactura int ,in codProducto int, in codBarra varchar(50), in producto varchar(50),in cantidad int, in precioVenta double, in total double, in idDetalle int)
begin
declare prod int;
declare cant int;

set prod=(select cantidad from detallefactura where codigoProducto=codProducto  and codigoDetalle=idDetalle);
update detallefactura set codigoFactura=codFactura ,codigoProducto=codProducto, codigoBarra=codBarra, nombreProducto=producto, cantidad= cantidad, precioVenta=precioVenta, total=total where codigoDetalle=idDetalle;
update factura set totalVenta=(SELECT SUM(total) FROM detallefactura WHERE codigoFactura=codFactura)where codigoFactura=codFactura;

IF prod>cantidad THEN
set cant=prod-cantidad;
update producto set stockActual=stockActual+cant where codigoProducto=codProducto;

ELSEIF prod<cantidad THEN 
set cant =cantidad-prod;
update producto set stockActual=stockActual-cant where codigoProducto=codProducto; 

END IF;
	
end$

delimiter $
create procedure delDetalle(in idDetalle int, in codFactura int, in codProducto int)
begin
declare cant int;
set cant =(select cantidad from detallefactura where codigoProducto=codProducto  and codigoDetalle=idDetalle);
delete from detalleFactura where codigoDetalle=idDetalle;
update factura set totalVenta=(SELECT SUM(total) FROM detallefactura WHERE codigoFactura=codFactura)where codigoFactura=codFactura;
update producto set stockActual=stockActual+cant where codigoProducto=codProducto;
end$

#ingresando Factura




#procedimientos almacenados

#insertar un detalle
