package com.conexion;
import java.sql.*;

/**
 * nombre de la clase: Conexion
 * fecha 25/09/18
 * version 1.0
 * copyright team developer's 1
 * @author josue
 */
public class Conexion {
    
    Connection con;

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }
    
    public void conectar() throws Exception
    {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/facturacion", "root", "");
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void desconectar() throws Exception
    {
        try {
            if (con!=null) {
                if (!con.isClosed()) {
                    con.close();
                }
            }
        } catch (Exception e) {
            throw e;
        }
    
    }
    
}
