
package com.controlador;

import com.google.gson.Gson;
import com.modelo.DaoVendedor;
import com.modelo.Vendedor;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * nombre ProcesarVendedor
 * fecha 25/09/18
 * version 1.0
 * copyright team developer's 1
 * @author josue
 */

public class ProcesarVendedor extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException,Exception {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Vendedor v = new Vendedor();
        DaoVendedor daoV = new DaoVendedor();
        
        String op = request.getParameter("key");
       
        
        int a = 0;
       
        switch(op)
        {
            case "verificarDui":
                  
                  a= daoV.verificarDui(request.getParameter("c"));
                  out.println(a);
                break;
            case "obtenerDataVendedor":
                  
                Vendedor ven =new Vendedor(); 
                ven.setCodigoVendedor(Integer.parseInt(request.getParameter("idVendedorM")));
                List<Vendedor> lista = daoV.getDataVendedor(ven);
                Gson json = new Gson();
                out.print(json.toJson(lista)); 
                break;
                
            case "modificar":
                
                v.setDui(request.getParameter("dui"));
                //out.print(v.getDui());
                v.setNombre(request.getParameter("nombres"));
                
                v.setApellidos(request.getParameter("apellidos"));
                
                v.setDireccion(request.getParameter("direccion"));
                
                v.setTelefonoMovil(request.getParameter("movil"));
                
                v.setTelefonoOficina(request.getParameter("oficina"));
                //out.print(v.getDireccion());
                v.setCodigoVendedor(Integer.parseInt(request.getParameter("a")));
                
                
                a = daoV.modificar(v); 
                
                out.print(a);
                
                
                
                break;
                
            case "agregar":
                v.setDui(request.getParameter("txtDui"));
                v.setNombre(request.getParameter("txtNombre"));
                v.setApellidos(request.getParameter("txtApellidos"));
                v.setDireccion(request.getParameter("txtDireccion"));
                v.setTelefonoMovil(request.getParameter("txtTelefonoMovil"));
                v.setTelefonoOficina(request.getParameter("txtTelOficina"));
                
                a = daoV.insertar(v); 
                out.println(a);
                
                break;
            case "eliminar":
                v.setCodigoVendedor(Integer.parseInt(request.getParameter("idVendedor")));
                
                a = daoV.eliminar(v); 
                out.println(a);
                
                break;
                
             default:
                 
                 break;
        }
         
         
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcesarVendedor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcesarVendedor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
