package com.controlador;

import com.modelo.Cliente;
import com.modelo.DaoCliente;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Nombre del Servlet: ProcesarCliente
 * Fecha: 03/10/2018
 * Version: 1.3.5
 * Copyright: lf_Fernando
 * @author Fernando Flamenco
 */
public class ProcesarCliente extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
         DaoCliente daocl = new DaoCliente();
        Cliente cl = new Cliente();

        if (request.getParameter("key").equals("insertar")) {
            try {

                cl.setCodigoCliente(0);
                cl.setNombre(request.getParameter("txtNombre"));
                cl.setApellidos(request.getParameter("txtApellido"));
                cl.setDireccion(request.getParameter("txtDireccion"));
                cl.setEstado(1);

                int respues = daocl.insertarCliente(cl);

                out.println(respues);
            } catch (Exception e) {
                out.println(0);
            }
        } else if (request.getParameter("key").equals("modificar")) {
            try {

                cl.setNombre(request.getParameter("txtNombreM"));
                cl.setApellidos(request.getParameter("txtApellidoM"));
                cl.setDireccion(request.getParameter("txtDireccionM"));
                cl.setCodigoCliente(Integer.parseInt(request.getParameter("txtCodigoM")));
                cl.setEstado(1);

                int respues = daocl.modificarCliente(cl);

                out.println(respues);
            } catch (Exception e) {
                out.println(0);
            }
        } else if (request.getParameter("key").equals("eliminar")) {

            try {
                cl.setCodigoCliente(Integer.parseInt(request.getParameter("idUsuario")));
                int respues = daocl.eliminarCliente(cl);

                out.println(respues);
            } catch (Exception e) {
                out.println(0);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
