/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.google.gson.Gson;
import com.modelo.DaoDetalleFactura;
import com.modelo.DaoFactura;
import com.modelo.DaoProducto;
import com.modelo.DetalleFactura;
import com.modelo.Factura;
import com.modelo.Producto;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Carlos_Campos
 */
public class ProcesarFacturacion extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
         HttpSession sesion=request.getSession();
          sesion.setAttribute("action", "");
               
       
                 
        Gson json= new Gson();
        DaoDetalleFactura daoD=new DaoDetalleFactura();
        Factura fac= new Factura();
        DaoFactura daoF= new DaoFactura();
        DetalleFactura det= new DetalleFactura();
        NumberFormat nf = NumberFormat.getInstance(Locale.US);
        
        DaoProducto daoP= new DaoProducto();
        
        String key=request.getParameter("key");
        
        
        
        switch(key){
        
            case "action":
               if(sesion.getAttribute("action")== null)
               {
                   sesion.setAttribute("action", "on");
               }
               else
               {
                   
               }
                
            
            break;
            case "cargarProducto":
               
               Producto p= new Producto(); 
              p.setCodigoProducto(Integer.parseInt(request.getParameter("idProducto")));
               List<Producto>listaProducto=daoP.extraerProducto(p);
               
               out.println(json.toJson(listaProducto));
                
            
            break;
            
            case "cargarFacturas":
               
               /*DaoDetalleFactura daod=new DaoDetalleFactura();
               
               fac.setCodigoFactura(Integer.parseInt(request.getParameter("idFactura")));
               List<DetalleFactura>listaDetalles=daod.mostrarDetalle(fac);
               
               out.print(json.toJson(listaDetalles));
                */
            
            break;
            case "insertar":
               //verficando si existe la factura
            
                
                //verificando 
                
                 int id=daoF.ObtenerIdFactuara(request.getParameter("numero"));
                 if(id==0)
                 {
                     try{
                fac.setCodigoCliente(Integer.parseInt(request.getParameter("cliente")));
                fac.setNumeroFactura(request.getParameter("numero"));
                fac.setFechaRegistro(request.getParameter("fecha"));
                fac.setCodigoVendedor(Integer.parseInt(request.getParameter("vendedor")));
                fac.setTotalVenta(0.0);
                
              
                daoF.insertarFactura(fac);
             
                 int codigo=daoF.ObtenerIdFactuara(request.getParameter("numero"));
                 
                 det.setCodigoFactura(codigo);
                 det.setCodigoProducto(Integer.parseInt(request.getParameter("idProducto")));
                 det.setNombreProducto(request.getParameter("nombre"));
                 det.setPrecioVenta(Double.parseDouble(request.getParameter("precio")));
                 det.setCodigoBarra(request.getParameter("codigoBarra"));
                 det.setCantidad(Integer.parseInt(request.getParameter("cantidad")));
                 int cantidad=Integer.parseInt(request.getParameter("cantidad"));
                 double precio=Double.parseDouble(request.getParameter("precio"));
                 double subTotal = cantidad*precio;
                 double total=subTotal+(subTotal*0.13);
                 det.setTotal(Double.parseDouble(nf.format(total)));
                 
                 int totalp=daoD.numProdyctos(Integer.parseInt(request.getParameter("idProducto")));
                
                if(Integer.parseInt(request.getParameter("cantidad"))>totalp)
                {
                    out.print("no");
                }
                else
                {
                     daoD.insertarDetalle(det);
                }
                
                 
                 
                 
                    sesion.setAttribute("action", "on");
                    sesion.setAttribute("id", codigo);
                    sesion.setAttribute("numero", request.getParameter("numero"));
                    sesion.setAttribute("cliente", request.getParameter("cliente") );
                    sesion.setAttribute("vendedor",  request.getParameter("vendedor"));
                    sesion.setAttribute("fecha",  request.getParameter("fecha"));
                }
                     catch(Exception e)
                     {
                         out.print(e);
                     }
                
                
                 }
                 else
                 {
                     try{
                 
                 int codigo=daoF.ObtenerIdFactuara(request.getParameter("numero"));
                 String numero=request.getParameter("numero");
                 det.setCodigoFactura(codigo);
                 det.setCodigoProducto(Integer.parseInt(request.getParameter("idProducto")));
                 det.setNombreProducto(request.getParameter("nombre"));
                 det.setPrecioVenta(Double.parseDouble(request.getParameter("precio")));
                 det.setCodigoBarra(request.getParameter("codigoBarra"));
                 det.setCantidad(Integer.parseInt(request.getParameter("cantidad")));
                 int cantidad=Integer.parseInt(request.getParameter("cantidad"));
                 double precio=Double.parseDouble(request.getParameter("precio"));
                 double subTotal = cantidad*precio;
                 double total=subTotal+(subTotal*0.13);
                 
                 det.setTotal(total);
                   int totalp=daoD.numProdyctos(Integer.parseInt(request.getParameter("idProducto")));
                 if(Integer.parseInt(request.getParameter("cantidad"))>totalp)
                {
                    out.print(1);
                }
                else
                {
                     daoD.insertarDetalle(det);
                     
                }
                 
                    sesion.setAttribute("action", "on");
                    sesion.setAttribute("action", "on");
                    sesion.setAttribute("id", codigo);
                    sesion.setAttribute("numero", request.getParameter("numero"));
                    sesion.setAttribute("cliente", request.getParameter("cliente") );
                    sesion.setAttribute("vendedor",  request.getParameter("vendedor"));
                    sesion.setAttribute("fecha",  request.getParameter("fecha"));
                
                 
                 
                 
                }
                     catch(Exception e)
                     {
                         out.print(e);
                     }
                
                 }
               
               
             
            
            break;
            case "generate":
               
                int resp=daoF.ObtenerUltima();
                out.print(resp+1);
               
               
            
            break;
            
            case "nuevaFactura":
               
                
                sesion.setAttribute("id", "");
                 sesion.setAttribute("numero", "");
                 sesion.setAttribute("cliente", "" );
                 sesion.setAttribute("vendedor", "");
                 sesion.setAttribute("fecha", "");
                 sesion.setAttribute("id", "");
                out.print("bien");
            
            break;
            case "getDetalle":
               
                    
                List<DetalleFactura> detalle=daoD.getDetalle(Integer.parseInt(request.getParameter("idUsuario")));
                out.print(json.toJson(detalle));
                
                
                
                
            
            break;
            
            case "modificar":
               
                try{
                 
                 det.setCodigoFactura(Integer.parseInt(request.getParameter("idFactura")));
                 det.setCodigoDetalle(Integer.parseInt(request.getParameter("idDetalle")));
                 det.setCodigoProducto(Integer.parseInt(request.getParameter("idProducto")));
                 det.setNombreProducto(request.getParameter("nombre"));
                 det.setPrecioVenta(Double.parseDouble(request.getParameter("precio")));
                 det.setCodigoBarra(request.getParameter("codigoBarra"));
                 det.setCantidad(Integer.parseInt(request.getParameter("cantidad")));
                 
                 
                 int cantidad=Integer.parseInt(request.getParameter("cantidad"));
                 double precio=Double.parseDouble(request.getParameter("precio"));
                 double subTotal = cantidad*precio;
                 double total=subTotal+(subTotal*0.13);
                 
                 det.setTotal(total);
                 
                 
               
                
                double totalV=daoD.totalFactura(Integer.parseInt(request.getParameter("idFactura")));
                out.print(totalV);
                
                
                
                
                daoD.modificarDetalle(det);
                out.print("bien");
                
                
                }
                
                catch (Exception e)
                {
                    out.print(e);
                }
            
            break;
            
                case "eliminar":
               
                try{
                 
                    
                    out.print(request.getParameter("idDetalle"));
                    out.print(request.getParameter("idFactura"));
                    det.setCodigoDetalle(Integer.parseInt(request.getParameter("idDetalle")));
                    det.setCodigoFactura(Integer.parseInt(request.getParameter("idFactura")));
                    det.setCodigoProducto(Integer.parseInt(request.getParameter("codProd")));
                    
                    daoD.eliminarDetalle(det);
                    out.print("bien");
                    
                 
                }
                
                catch (Exception e)
                {
                    out.print(e);
                }
            
            break;
        }
              
        
        
        
        
        
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ProcesarFacturacion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ProcesarFacturacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ProcesarFacturacion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ProcesarFacturacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
