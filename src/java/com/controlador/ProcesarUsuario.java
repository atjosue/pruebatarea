/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.modelo.DaoUsuario;
import com.modelo.Usuario;
import com.resourses.Encriptacion;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Carlos_Campos
 */
public class ProcesarUsuario extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        RequestDispatcher rd= null;
        //instaciando implementos
        Encriptacion enc= new Encriptacion();
        Usuario us= new Usuario();
        DaoUsuario daou= new DaoUsuario();
        
        
        //extrayendo variables
        String key=request.getParameter("key");
        
        switch(key)
        {
            case "log":
            String nombre=request.getParameter("nombre");
            String clave=request.getParameter("clave");
            String clave1=enc.SHA1(clave);
            String claveFinal=enc.MD5(clave1);
            
          
            us.setNombreUsuario(nombre);
            us.setClave(claveFinal);
            
            int resp=daou.Acceder(us);
            if(resp!=0)
            {
            HttpSession sesion=request.getSession();
            int nivel=0;
            
            nivel=resp;
            
            sesion.setAttribute("nombre",request.getParameter("nombre") );
            sesion.setAttribute("nivel", nivel);
           
        }
        out.println(resp);

         break; 
        }
        
       
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcesarUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcesarUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
