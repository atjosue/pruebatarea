
package com.controlador;

import com.modelo.DaoProducto;
import com.modelo.Producto;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Nombre Del servlet: ProcesarProducto
 * @author Kevin Antonio Montiel Ramos
 * Fecha: 07/10/2018
 * Version: 1.0
 * Copyrigth: KAMR
 */

@WebServlet(name = "ProcesarProducto", urlPatterns = {"/procesarProducto"})
public class ProcesarProducto extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        DaoProducto daop = new DaoProducto();
        Producto pro = new Producto();
        
        if(request.getParameter("key").equals("insertar"))
        {
            try 
            {
                pro.setCodigoProducto(0);
                pro.setNombreProducto(request.getParameter("txtNombre"));
                pro.setPrecioVenta(Double.parseDouble(request.getParameter("txtPrecio")));
                pro.setStockMinimo(Integer.parseInt(request.getParameter("txtStockM")));
                pro.setStockActual(Integer.parseInt(request.getParameter("txtStockA")));
                pro.setCodigoBarra(request.getParameter("txtCodigoB"));
                pro.setEstado(Integer.parseInt(request.getParameter("txtEstado")));
                
                int resp = daop.insertarCliente(pro);
                
                out.println(resp);
                
            }
            catch (Exception e) 
            {
                out.println(0);
            }
        }
        else if(request.getParameter("key").equals("modificar"))
        {
            try 
            {
                pro.setNombreProducto(request.getParameter("txtNombreM"));
                pro.setPrecioVenta(Double.parseDouble(request.getParameter("txtPrecioM")));
                pro.setStockMinimo(Integer.parseInt(request.getParameter("txtStockMM")));
                pro.setStockActual(Integer.parseInt(request.getParameter("txtStockAM")));
                pro.setCodigoBarra(request.getParameter("txtCodigoBM"));
                pro.setCodigoProducto(Integer.parseInt(request.getParameter("txtCodigoM")));
                
                int resp = daop.modificarProducto(pro);
                
                out.println(resp);
            }
            catch (Exception e) 
            {
                out.println(0);
            }
        }
        else if(request.getParameter("key").equals("eliminar"))
        {
            try 
            {
                pro.setCodigoProducto(Integer.parseInt(request.getParameter("idProducto")));
                
                int resp = daop.eliminarProducto(pro);
                
                out.println(resp);
            } 
            catch (Exception e)
            {
                out.println(0);
            }
        }        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
