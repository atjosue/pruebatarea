package com.modelo;

import com.conexion.Conexion;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;

/**
 * Nombre de la clase: DaoUsuario
 * Fecha: 27/09/2018
 * Version: 1.0
 * copyRight Carlos Campos
 * @author Carlos_Campos
 */
public class DaoUsuario extends Conexion {
 
    //extrayendo lista de Usuarios
    
    public List<Usuario> mostrarUsuarios() throws ClassNotFoundException, SQLException, Exception
    {
        List<Usuario>listaUsuarios= new ArrayList();
        ResultSet res;
        try 
        {
            this.conectar();
            String sql="select * from usuario where estado='1';";
            PreparedStatement pre= this.getCon().prepareStatement(sql);
            res=pre.executeQuery();
            while(res.next())
            {
                Usuario us= new Usuario();
                us.setCodigoUsuario(res.getInt("codigoUsuario"));
                us.setNombre(res.getString("nombre"));
                us.setNombreUsuario(res.getString("nombreUsuario"));
                us.setClave(res.getString("clave"));
                us.setCodigoRol(res.getInt("codigoRol"));
                listaUsuarios.add(us);
            }
        }
        catch (ClassNotFoundException | SQLException e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        
        return listaUsuarios;
          
    }
    
    //Metodo para hacer login
    public int  Acceder(Usuario us) throws Exception
    {
        int rol=0;
        ResultSet res;
         try
        {
            this.conectar();
            String sql="select codigoRol from usuario where nombreUsuario=? and clave=?";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setString(1, us.getNombreUsuario());
            pre.setString(2, us.getClave());
            res=pre.executeQuery();
            
            while(res.next())
            {
                rol=res.getInt("codigoRol");
            }
        }
         catch (SQLException e)
         {
             throw e;
        }
         finally
         {
         this.desconectar();
         }
    
            return rol;
    }
    
    //obtener nombre del nivel
        
        public String getNivel(int cod) throws Exception
        {
            String nivel="";
            ResultSet res;
                try {
                    this.conectar();
                    String sql="select nombreRol from rol where codigoRol=?";
                    PreparedStatement pre = this.getCon().prepareStatement(sql);
                    pre.setInt(1, cod);
                    res=pre.executeQuery();
                    while(res.next())
                    {
                        nivel=res.getString("nombreRol");
                    }
                } catch (Exception e) {
                    throw e;
                }finally
                {
                    this.desconectar();
                }
            
            return nivel;
        }
        
    //nombre nivel por nombre
        public int getCodigoNivel(String cod) throws Exception
        {
            int nivel=0;
            ResultSet res;
                try {
                    this.conectar();
                    String sql="select codigoRol from rol where nombreRol=?";
                    PreparedStatement pre = this.getCon().prepareStatement(sql);
                    pre.setString(1, cod);
                    res=pre.executeQuery();
                    while(res.next())
                    {
                        nivel=res.getInt("codigoRol");
                    }
                } catch (Exception e) {
                    throw e;
                }finally
                {
                    this.desconectar();
                }
            
            return nivel;
        }
    
    //obtener niveles
    
    /*public List<TipoUsuario> mostrarRol() throws ClassNotFoundException, SQLException, Exception
    {
        List<TipoUsuario>listaRoles= new ArrayList();
        ResultSet res;
        try 
        {
            this.conectar();
            String sql="select * from rol";
            PreparedStatement pre= this.getCon().prepareStatement(sql);
            res=pre.executeQuery();
            while(res.next())
            {
                TipoUsuario tp= new TipoUsuario();
                tp.setCodigoRol(res.getInt("codigoRol"));
                tp.setNombreRol(res.getString("nombreRol"));
                listaRoles.add(tp);
            }
        }
        catch (ClassNotFoundException | SQLException e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        
        return listaRoles;
          
    }
    
        //verificar usuario
        // "d" es la variable con la cual le decimos si la validacion es para nuevo 
        // usuario o para una modificacion.
        public int verificarUsuario(String usua) throws Exception
        {
            int r=0;
            
            ResultSet res;
                try {
                    this.conectar();
                    String sql="select nombreUsuario from usuario where nombreUsuario=?";
                    PreparedStatement pre = this.getCon().prepareStatement(sql);
                    pre.setString(1, usua);
                    res=pre.executeQuery();
                    
                    
                        while(res.next())
                        {
                            r=2;
                        }
                    
                } catch (Exception e) {
                    throw e;
                }finally
                {
                    this.desconectar();
                }
            
            return r;
        }
        public String verificarUsuario2(String usua,int codUs) throws Exception
        {
            
            String usss="";
            
            
            ResultSet res;
                try {
                    this.conectar();
                    String sql="select nombreUsuario from usuario where nombreUsuario=? and codigoUsuario=?;";
                    PreparedStatement pre = this.getCon().prepareStatement(sql);
                    pre.setString(1, usua);
                    pre.setInt(1, codUs);
                    res=pre.executeQuery();
                        while(res.next())
                        {
                            Usuario u= new Usuario();
                            usss=(res.getString("nombreUsuario"));
                            
                        }
                } catch (Exception e) {
                    throw e;
                }finally
                {
                    this.desconectar();
                }
                
            
            return usss ;
        }
        
        public int agregarUsuario(Usuario u) throws Exception
        {
            int r=1;
                
                try {
                    this.conectar();
                    String sql="insert into usuario values(?,?,?,?,?,?)";
                    PreparedStatement pre = this.getCon().prepareStatement(sql);
                    pre.setInt(1, 0);
                    pre.setString(2,u.getNombre());
                    pre.setString(3, u.getNombreUsuario());
                    pre.setString(4, u.getClave());
                    pre.setInt(5,u.getCodigoRol());
                    pre.setInt(6, 1);
                    r=pre.executeUpdate();
                } catch (Exception e) {
                    
                    throw e;
                    
                }finally
                {
                    this.desconectar();
                }
            
            return r;
        }
        
        public int modificarUsuario(Usuario u) throws Exception
        {
            int r=0;
                
                try {
                    this.conectar();
                    String sql="UPDATE usuario SET nombre =?, nombreUsuario=?, clave=?, codigoRol=? WHERE codigoUsuario=?;";
                    PreparedStatement pre = this.getCon().prepareStatement(sql);
                    pre.setString(1,u.getNombre());
                    pre.setString(2, u.getNombreUsuario());
                    pre.setString(3, u.getClave());
                    pre.setInt(4,u.getCodigoRol());
                    pre.setInt(5, u.getCodigoUsuario());
                    r=pre.executeUpdate();
                } catch (Exception e) {
                    
                    throw e;
                    
                }finally
                {
                    this.desconectar();
                }
            
            return r;
        }
        public int eliminarUsuario(Usuario u) throws Exception
        {
            int r=1;
                
                try {
                    this.conectar();
                    String sql="UPDATE usuario SET estado =? WHERE codigoUsuario=?;";
                    PreparedStatement pre = this.getCon().prepareStatement(sql);
                    pre.setInt(1,0);
                    pre.setInt(2, u.getCodigoUsuario());
                    r=pre.executeUpdate();
                } catch (Exception e) {
                    
                    throw e;
                    
                }finally
                {
                    this.desconectar();
                }
            
            return r;
        }
        
        public List<Usuario> getDataUsuario(int cod) throws ClassNotFoundException, SQLException, Exception
    {
        List<Usuario>info= new ArrayList();
        ResultSet res;
        try 
        {
            this.conectar();
            String sql="select * from usuario where codigoUsuario=?";
            PreparedStatement pre= this.getCon().prepareStatement(sql);
            pre.setInt(1, cod );
            res=pre.executeQuery();
            while(res.next())
            {
                Usuario us = new Usuario();
                us.setNombre(res.getString("nombre"));
                us.setNombreUsuario(res.getString("nombreUsuario"));
                us.setClave(res.getString("clave"));
                us.setCodigoRol(res.getInt("codigoRol"));
                us.setCodigoUsuario(res.getInt("codigoUsuario"));
                info.add(us);
            }
        }
        catch (ClassNotFoundException | SQLException e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        
        return info;
          
    }
        
}
