
package com.modelo;
import com.conexion.Conexion;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
 

/**
 * Nombre de la clase: DaoFactura
 * Fecha: 27/09/2018
 * Version: 1.0
 * copyRight Carlos Campos
 * @author Carlos_Campos
 */
public class DaoFactura  extends Conexion{
    
    public List<Factura> mostrarFacturas() throws ClassNotFoundException, SQLException, Exception
    {
        List<Factura>listaFacturas= new ArrayList();
        ResultSet res;
        try 
        {
            this.conectar();
            String sql="select * from factura";
            PreparedStatement pre= this.getCon().prepareStatement(sql);
            res=pre.executeQuery();
            while(res.next())
            {
                Factura fac= new Factura();
                fac.setCodigoFactura(res.getInt("codigoFactura"));
                fac.setNumeroFactura(res.getString("numeroFactura"));
                fac.setCodigoVendedor(res.getInt("codigoVendedor"));
                fac.setCodigoCliente(res.getInt("codigoCliente"));
                fac.setTotalVenta(res.getDouble("totalVenta"));
                fac.setFechaRegistro(res.getString("fechaRegistro"));
                
                listaFacturas.add(fac);
            }
        }
        catch (ClassNotFoundException | SQLException e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        
        return listaFacturas;
          
    }
    
    
     public void insertarFactura(Factura fac) throws Exception
    {
        
        try 
        {
            this.conectar();
            String sql="{call addFactura(?,?,?,?,?)}";
            PreparedStatement pre=this.getCon().prepareStatement(sql);
            pre.setString(1, fac.getNumeroFactura());
            pre.setInt(2, fac.getCodigoVendedor());
            pre.setInt(3, fac.getCodigoCliente());
            pre.setDouble(4, fac.getTotalVenta());
            pre.setString(5, fac.getFechaRegistro());
           
            pre.executeUpdate();
          
        } 
        catch (SQLException e) 
        {
           throw e;
        }
        finally
        {
            this.desconectar();
        }
    
    
    }
     
     public void modificarFactura(Factura fac) throws Exception
    {
        
        try 
        {
            this.conectar();
            String sql="{call updateFactura(?,?,?,?,?,?)}";
            PreparedStatement pre=this.getCon().prepareStatement(sql);
            pre.setString(1, fac.getNumeroFactura());
            pre.setInt(2, fac.getCodigoVendedor());
            pre.setInt(3, fac.getCodigoCliente());
            pre.setDouble(4, fac.getTotalVenta());
            pre.setString(5, fac.getFechaRegistro());
            pre.setInt(6, fac.getCodigoFactura());
           
            pre.executeUpdate();
          
        } 
        catch (SQLException e) 
        {
           throw e;
        }
        finally
        {
            this.desconectar();
        }
    
    
    }
    
      public void eliminarFactura(Factura fac) throws Exception
    {
        
        try 
        {
            this.conectar();
            String sql="{call delFactura(?)}";
            PreparedStatement pre=this.getCon().prepareStatement(sql);
            pre.setInt(1, fac.getCodigoFactura());
           
            pre.executeUpdate();
          
        } 
        catch (SQLException e) 
        {
           throw e;
        }
        finally
        {
            this.desconectar();
        }
    
    
    }
      
   public int ObtenerUltima()
   {
       
       int lastCode=0;
       ResultSet res;
        try
       {
            this.conectar();
            String sql="select codigoFactura from factura where codigoFactura=(select MAX(codigoFactura) from factura)";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            res=pre.executeQuery();
            
            while(res.next())
            {
                lastCode=res.getInt("codigoFactura");
            }
       }
        catch (Exception e) 
       {
           
       }
        
        return lastCode;
   }
   
   
   //obtner al id de la factura
   
    
    
    public int ObtenerIdFactuara(String numFactura)
   {
       
       int codigoFactura=0;
       ResultSet res;
        try
       {
            this.conectar();
            String sql="select codigoFactura from factura where numeroFactura=?";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setString(1, numFactura);
            res=pre.executeQuery();
            
            while(res.next())
            {
                codigoFactura=res.getInt("codigoFactura");
            }
       }
        catch (Exception e) 
       {
           
       }
        
        return codigoFactura;
   }
    
    
}
