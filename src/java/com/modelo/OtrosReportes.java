package com.modelo;
import com.conexion.Conexion;
import java.sql.*;

/**
 * Nombre de la Clase: OtrosReportes
 * Fecha: 03/10/2018
 * Version: 1.0
 * Copyright: lf_Flamenco
 * @author Fernando Flamenco
 */
public class OtrosReportes extends Conexion{
    
    public int numCliente() throws Exception
    {
        int totalClientes=0;
        ResultSet res;
        try 
        {
            this.conectar();
            String sql="select count(*) as total from cliente where estado=1;";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            res = pre.executeQuery();
            while(res.next())
            {
                  totalClientes = res.getInt(1);
            }
        }    
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        
        return totalClientes;
    }
    
    public int numUsuarios() throws Exception
    {
        int totalClientes=0;
        ResultSet res;
        try 
        {
            this.conectar();
            String sql="select count(*) as total from usuario;";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            res = pre.executeQuery();
            while(res.next())
            {
                  totalClientes = res.getInt(1);
            }
        }    
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        
        return totalClientes;
    }
    
    public int numProductos() throws Exception
    {
        int totalClientes=0;
        ResultSet res;
        try 
        {
            this.conectar();
            String sql="select count(*) as total from producto";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            res = pre.executeQuery();
            while(res.next())
            {
                  totalClientes = res.getInt(1);
            }
        }    
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        
        return totalClientes;
    }
    
    public int numVentas() throws Exception
    {
        int totalVentas=0;
        
        ResultSet res;
        try 
        {
            this.conectar();
            String sql="select count(*) as total from factura";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            res = pre.executeQuery();
            while(res.next())
            {
                  totalVentas = res.getInt(1);
            }
        }    
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        
        return totalVentas;
    }
    
}
