
package com.modelo;

/**
 * nombre de la clase: Vendedor
 * fecha 25/09/18
 * version 1.0
 * copyright team developer's 1
 * @author josue
 */
public class Vendedor {
   private int codigoVendedor;
   private String dui; 
   private String nombre;
   private String apellidos;
   private String direccion;
   private String telefonoOficina;
   private String telefonoMovil;
   private int estado;

    public Vendedor() {
    }

    public Vendedor(int codigoVendedor, String dui, String nombre, String apellidos, String direccion, String telefonoOficina, String telefonoMovil, int estado) {
        this.codigoVendedor = codigoVendedor;
        this.dui = dui;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.telefonoOficina = telefonoOficina;
        this.telefonoMovil = telefonoMovil;
        this.estado = estado;
    }

    public int getCodigoVendedor() {
        return codigoVendedor;
    }

    public void setCodigoVendedor(int codigoVendedor) {
        this.codigoVendedor = codigoVendedor;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefonoOficina() {
        return telefonoOficina;
    }

    public void setTelefonoOficina(String telefonoOficina) {
        this.telefonoOficina = telefonoOficina;
    }

    public String getTelefonoMovil() {
        return telefonoMovil;
    }

    public void setTelefonoMovil(String telefonoMovil) {
        this.telefonoMovil = telefonoMovil;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
   
   
    
   
   
   
}
