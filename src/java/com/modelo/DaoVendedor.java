
package com.modelo;
import com.conexion.Conexion;
import java.util.*;
import java.sql.*;

/**
 * nombre de la clase DaoVendedor
 * fecha 25/09/18
 * version 1.0
 * copyright team developer's 1
 * @author josue
 */
public class DaoVendedor extends Conexion {
    
    public List<Vendedor> mostrarVendedores() throws Exception
    {
        List<Vendedor> listado = new ArrayList();
        ResultSet res;
        try {
            this.conectar();
            String sql="select * from vendedor where estado='1';";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            res=pre.executeQuery();
            while (res.next()) {                
                Vendedor v = new Vendedor();
                v.setCodigoVendedor(res.getInt("codigoVendedor"));
                v.setNombre(res.getString("nombre"));
                v.setApellidos(res.getString("apellidos"));
                v.setDui(res.getString("dui"));
                v.setDireccion(res.getString("direccion"));
                v.setTelefonoMovil(res.getString("telefonoMovil"));
                v.setTelefonoOficina(res.getString("telefonoOficina"));
                listado.add(v);
            }
        } catch (Exception e) {
            throw e;
        }
        return listado;
    }
    
    public int insertar(Vendedor v) throws Exception
    {
        int a=0;
        System.out.println(v.getApellidos());
        
        try {
            this.conectar();
            String sql="INSERT INTO vendedor  VALUES (?,?,?,?,?,?,?,?);";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setInt(1, 0);
            pre.setString(2,v.getDui());
            pre.setString(3, v.getNombre());
            pre.setString(4,v.getApellidos());
           pre.setString(5, v.getDireccion());
            pre.setString(6,v.getTelefonoOficina() );
           pre.setString(7, v.getTelefonoMovil());
           pre.setInt(8, 1);
            a = pre.executeUpdate();
            
        } catch (Exception e) {
            throw  e;
        }   
        return a;
    }
    
    public int modificar(Vendedor v) throws Exception
    {
        int res=0;
        try {
            this.conectar();
            String sql="UPDATE vendedor SET dui=?, nombre=?,apellidos=?, direccion=?,telefonoOficina=?,telefonoMovil=? WHERE codigoVendedor=?;";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setString(1,v.getDui());
            pre.setString(2, v.getNombre());
            pre.setString(3,v.getApellidos());
           pre.setString(4, v.getDireccion());
            pre.setString(5,v.getTelefonoOficina() );
           pre.setString(6, v.getTelefonoMovil());
           pre.setInt(7, v.getCodigoVendedor());
           res=pre.executeUpdate();
        } catch (Exception e) {
            throw  e;
        }   
        return res;
    }
    public int eliminar(Vendedor v) throws Exception
    {
        int resul=0;
        try {
            this.conectar();
            String sql="update vendedor set estado='0' WHERE codigoVendedor=?;";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setInt(1, v.getCodigoVendedor());
            resul=pre.executeUpdate();
           
        } catch (Exception e) {
            throw  e;
        }  
        
        return resul;
    }
    
    public int verificarDui(String dui) throws Exception
    {
        int resp=0;
        ResultSet res;
            try {
                this.conectar();
                String sql="select * from vendedor where dui=?";
                PreparedStatement pre = this.getCon().prepareStatement(sql);
                pre.setString(1, dui);
                res=pre.executeQuery();
                while(res.next())
                {
                    resp=1;
                }
            } catch (Exception e) 
            {
                throw e;
            }
        return resp; 
    }
    
    public List<Vendedor> getDataVendedor(Vendedor v) throws Exception
    {
    List<Vendedor> info = new ArrayList();
    ResultSet res;    
        try {
                this.conectar();
                String sql="select * from vendedor where codigoVendedor=?";
                PreparedStatement pre = this.getCon().prepareStatement(sql);
                pre.setInt(1, v.getCodigoVendedor() );
                res=pre.executeQuery();
                while(res.next())
                {
                    Vendedor ve = new Vendedor();
                    ve.setCodigoVendedor(res.getInt("codigoVendedor"));
                    ve.setDui(res.getString("dui"));
                    ve.setNombre(res.getString("nombre"));
                    ve.setApellidos(res.getString("apellidos"));
                    ve.setDireccion(res.getString("direccion"));
                    ve.setTelefonoMovil(res.getString("telefonoMovil"));
                    ve.setTelefonoOficina(res.getString("telefonoOficina"));
                    info.add(ve);
                }
            } catch (Exception e) {
                throw e;
            }
        
     return info;
    }
    
}
