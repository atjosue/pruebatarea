package com.modelo;

/**
 * Nombre de la clase: Cliente
 * Fecha: 01/10/2018
 * Version: 1.1
 * Copyright: lf_Flamenco
 * @author Fernando Flamenco
 */
public class Cliente {
    
    private int codigoCliente;
    private String nombre;
    private String apellidos;
    private String direccion;
    private int estado;

    public Cliente() {
    }

    public Cliente(int codigoCliente, String nombre, String apellidos, String direccion, int estado) {
        this.codigoCliente = codigoCliente;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.estado = estado;
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
    
    
}
