package com.modelo;

import com.conexion.Conexion;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carlos_Campos
 */
public class DaoDetalleFactura extends Conexion{
    
    
    public List<DetalleFactura> mostrarDetalle(int codigo) throws ClassNotFoundException, SQLException, Exception
    {
        List<DetalleFactura>listaDetalle= new ArrayList();
        ResultSet res;
        try 
        {
            this.conectar();
            String sql="select * from detallefactura where codigoFactura=?";
            PreparedStatement pre= this.getCon().prepareStatement(sql);
            pre.setInt(1, codigo);
            res=pre.executeQuery();
            
            while(res.next())
            {
                DetalleFactura dfac= new DetalleFactura();
                dfac.setCodigoDetalle(res.getInt("codigoDetalle"));
                dfac.setCodigoFactura(res.getInt("codigoFactura"));
                dfac.setCodigoProducto(res.getInt("codigoProducto"));
                dfac.setCodigoBarra(res.getString("codigoBarra"));
                dfac.setNombreProducto(res.getString("nombreProducto"));
                dfac.setCantidad(res.getInt("cantidad"));
                dfac.setPrecioVenta(res.getDouble("precioVenta"));
                dfac.setTotal(res.getDouble("total"));
                listaDetalle.add(dfac);
            }
        }
        catch (ClassNotFoundException | SQLException e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        
        return listaDetalle;
          
    }
    
    
    //agregando detalle}
    public void insertarDetalle(DetalleFactura det) throws Exception
    {
        
        try 
        {
            this.conectar();
            String sql="{call addDetalle(?,?,?,?,?,?,?)}";
            PreparedStatement pre=this.getCon().prepareStatement(sql);
            pre.setInt(1, det.getCodigoFactura());
            pre.setInt(2, det.getCodigoProducto());
            pre.setString(3, det.getCodigoBarra());
            pre.setString(4, det.getNombreProducto());
            pre.setInt(5, det.getCantidad());
            pre.setDouble(6, det.getPrecioVenta());
            pre.setDouble(7, det.getTotal());
            
           
            pre.executeUpdate();
          
        } 
        catch (SQLException e) 
        {
           throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    
    //modificando Detalle
    public void modificarDetalle(DetalleFactura det) throws Exception
    {
        
        try 
        {
            this.conectar();
            String sql="{call updateDetalle(?,?,?,?,?,?,?,?)}";
            PreparedStatement pre=this.getCon().prepareStatement(sql);
            pre.setInt(1, det.getCodigoFactura());
            pre.setInt(2, det.getCodigoProducto());
            pre.setString(3, det.getCodigoBarra());
            pre.setString(4, det.getNombreProducto());
            pre.setInt(5, det.getCantidad());
            pre.setDouble(6, det.getPrecioVenta());
            pre.setDouble(7, det.getTotal());
            pre.setInt(8, det.getCodigoDetalle());
            
            
           
            pre.executeUpdate();
            
          
        } 
        catch (SQLException e) 
        {
           throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    
    //Eliminando detalle
    public void eliminarDetalle(DetalleFactura det) throws Exception
    {
        
        try 
        {
            this.conectar();
            String sql="{call delDetalle(?,?,?)}";
            PreparedStatement pre=this.getCon().prepareStatement(sql);
             pre.setInt(1,det.getCodigoDetalle() );
            pre.setInt(2,det.getCodigoFactura());
            pre.setInt(3, det.getCodigoProducto());
           
            
           
            pre.executeUpdate();
          
        } 
        catch (SQLException e) 
        {
           throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
     public List<DetalleFactura> getDetalle(int codigo) throws ClassNotFoundException, SQLException, Exception
    {
        List<DetalleFactura>listaDetalle= new ArrayList();
        ResultSet res;
        try 
        {
            this.conectar();
            String sql="select * from detallefactura where codigoDetalle=?";
            PreparedStatement pre= this.getCon().prepareStatement(sql);
            pre.setInt(1, codigo);
            res=pre.executeQuery();
            
            while(res.next())
            {
                DetalleFactura dfac= new DetalleFactura();
                dfac.setCodigoDetalle(res.getInt("codigoDetalle"));
                dfac.setCodigoFactura(res.getInt("codigoFactura"));
                dfac.setCodigoProducto(res.getInt("codigoProducto"));
                dfac.setCodigoBarra(res.getString("codigoBarra"));
                dfac.setNombreProducto(res.getString("nombreProducto"));
                dfac.setCantidad(res.getInt("cantidad"));
                dfac.setPrecioVenta(res.getDouble("precioVenta"));
                dfac.setTotal(res.getDouble("total"));
                listaDetalle.add(dfac);
            }
        }
        catch (ClassNotFoundException | SQLException e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        
        return listaDetalle;
          
    }
    public double totalFactura(int codigo) throws ClassNotFoundException, SQLException, Exception
    {
        List<DetalleFactura>listaDetalle= new ArrayList();
        ResultSet res;
        double total =0;
        try 
        {
            this.conectar();
            String sql="SELECT SUM(total) as total FROM detallefactura WHERE codigoFactura=?;";
            PreparedStatement pre= this.getCon().prepareStatement(sql);
            pre.setInt(1, codigo);
            res=pre.executeQuery();
            
            while(res.next())
            {
                total=res.getDouble("total");
            }
        }
        catch (ClassNotFoundException | SQLException e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        
        return total;
          
    }
    
        public int numProdyctos(int idProd) throws ClassNotFoundException, SQLException, Exception
    {
       
        ResultSet res;
        int actual =0;
        int minimo=0;
        int total=0;
        try 
        {
            this.conectar();
            String sql="SELECT stockActual, stockMinimo FROM producto WHERE codigoProducto=?;";
            PreparedStatement pre= this.getCon().prepareStatement(sql);
            pre.setInt(1, idProd);
            res=pre.executeQuery();
            
            while(res.next())
            {
                actual=res.getInt("stockActual");
                minimo=res.getInt("stockMinimo");
                
                total=actual-minimo;
                
            }
        }
        catch (ClassNotFoundException | SQLException e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        
        return total;
          
    }
    
    
}
