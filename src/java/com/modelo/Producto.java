package com.modelo;

/**
 * Nombre De La Clase: Producto
 * @author Kevin Antonio Montiel Ramos
 * Fecha: 02/10/2018
 * Version: 1.0
 * Copyrigth: KAMR
 */
public class Producto 
{   
    private int codigoProducto;
    private String nombreProducto;
    private double precioVenta;
    private int stockMinimo;
    private int stockActual;
    private String codigoBarra;
    private int estado;

    public Producto() {
    }

    public Producto(int codigoProducto, String nombreProducto, double precioVenta, int stockMinimo, int stockActual, String codigoBarra, int estado) {
        this.codigoProducto = codigoProducto;
        this.nombreProducto = nombreProducto;
        this.precioVenta = precioVenta;
        this.stockMinimo = stockMinimo;
        this.stockActual = stockActual;
        this.codigoBarra = codigoBarra;
        this.estado = estado;
    }

    public int getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }

    public int getStockMinimo() {
        return stockMinimo;
    }

    public void setStockMinimo(int stockMinimo) {
        this.stockMinimo = stockMinimo;
    }

    public int getStockActual() {
        return stockActual;
    }

    public void setStockActual(int stockActual) {
        this.stockActual = stockActual;
    }

    public String getCodigoBarra() {
        return codigoBarra;
    }

    public void setCodigoBarra(String codigoBarra) {
        this.codigoBarra = codigoBarra;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

   
    
}

