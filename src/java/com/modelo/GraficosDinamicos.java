
package com.modelo;
import com.conexion.Conexion;
import com.google.gson.Gson;
import java.sql.*;

/**
 *
 * @author Fernando
 */
public class GraficosDinamicos extends Conexion {

    public String grafica1() {
        String[] data= new String[2];
        ResultSet res;
        Gson json = new Gson();
        try {
            this.conectar();
            String sql = "SELECT d.nombreProducto AS producto , SUM(d.cantidad) AS cantidad\n"
                    + "FROM detallefactura d\n"
                    + "GROUP BY d.nombreProducto\n"
                    + "ORDER BY SUM(d.cantidad) DESC\n"
                    + "LIMIT 0 , 3";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            res = pre.executeQuery();
            while(res.next())
            {
                data[0]= res.getString(1);
                data[1]= res.getString(2);
               
         
                
            }
            
        } catch (Exception e) {

        } finally {

        }
        return json.toJson(data);
    }

}
