package com.modelo;

/**
 * Nombre de la clase: Usuario
 * Fecha: 27/09/2018
 * Version: 1.0
 * copyRight Carlos Campos
 * @author Carlos_Campos
 */
public class Usuario {
    
    private int codigoUsuario;
    private String nombre;
    private String nombreUsuario;
    private String clave;
    private int codigoRol;
    private int estado;

    public Usuario() {
    }

    public Usuario(int codigoUsuario, String nombre, String nombreUsuario, String clave, int codigoRol, int estado) {
        this.codigoUsuario = codigoUsuario;
        this.nombre = nombre;
        this.nombreUsuario = nombreUsuario;
        this.clave = clave;
        this.codigoRol = codigoRol;
        this.estado = estado;
    }

    public int getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public int getCodigoRol() {
        return codigoRol;
    }

    public void setCodigoRol(int codigoRol) {
        this.codigoRol = codigoRol;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    
}
