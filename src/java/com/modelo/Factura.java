
package com.modelo;

/**
 * Nombre de la clase: Factura
 * Fecha: 27/09/2018
 * Version: 1.0
 * copyRight Carlos Campos
 * @author Carlos_Campos
 */
public class Factura {
   
    private int  codigoFactura;
    private String numeroFactura;
    private int codigoVendedor;
    private int codigoCliente;
    private double totalVenta;
    private String fechaRegistro;

    public Factura() {
    }

    public Factura(int codigoFactura, String numeroFactura, int codigoVendedor, int codigoCliente, double totalVenta, String fechaRegistro) {
        this.codigoFactura = codigoFactura;
        this.numeroFactura = numeroFactura;
        this.codigoVendedor = codigoVendedor;
        this.codigoCliente = codigoCliente;
        this.totalVenta = totalVenta;
        this.fechaRegistro = fechaRegistro;
    }

    public int getCodigoFactura() {
        return codigoFactura;
    }

    public void setCodigoFactura(int codigoFactura) {
        this.codigoFactura = codigoFactura;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public int getCodigoVendedor() {
        return codigoVendedor;
    }

    public void setCodigoVendedor(int codigoVendedor) {
        this.codigoVendedor = codigoVendedor;
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public double getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(double totalVenta) {
        this.totalVenta = totalVenta;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
    
    
    
    
}
