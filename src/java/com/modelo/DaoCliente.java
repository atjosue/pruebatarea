package com.modelo;
import java.util.*;
import java.sql.*;
import com.modelo.*;
import com.conexion.Conexion;

/**
 * Nombre de la clase: DaoCliente
 * Fecha: 30/09/18
 * Version: 1.0
 * Copyright: team developer's 1
 * @author Fernando Flamenco
 */
public class DaoCliente extends Conexion{
    
    public List<Cliente>mostrarClientes() throws ClassNotFoundException, SQLException, Exception
    {
        List<Cliente> mostrarClientes = new ArrayList();
        ResultSet res;
        
        try 
        {
            this.conectar();
            String sql="select * from cliente where estado=1";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            res=pre.executeQuery();
            
            while(res.next())
            {
                Cliente cl = new Cliente();
                cl.setCodigoCliente(res.getInt("codigoCliente"));
                cl.setNombre(res.getString("nombre"));
                cl.setApellidos(res.getString("apellidos"));
                cl.setDireccion(res.getString("direccion"));
                
                mostrarClientes.add(cl);
            }
        } 
        catch (SQLException | ClassNotFoundException e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        
        return mostrarClientes;
    }
    
    public int insertarCliente(Cliente cl) throws Exception
    {
        int resu;
        try 
        {
            this.conectar();
            String sql="insert into cliente values(?,?,?,?,?);";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setInt(1, cl.getCodigoCliente());
            pre.setString(2, cl.getNombre());
            pre.setString(3, cl.getApellidos());
            pre.setString(4, cl.getDireccion());
            pre.setInt(5, cl.getEstado());
           
            resu =  pre.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return resu;
    }
    
    public int modificarCliente(Cliente cl) throws Exception
    {
        int res;
        try 
        {
            this.conectar();
            String sql = "update cliente set nombre=?, apellidos=?, direccion=?, estado=? where codigoCliente=?";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setString(1, cl.getNombre());
            pre.setString(2, cl.getApellidos());
            pre.setString(3, cl.getDireccion());
            pre.setInt(4, cl.getEstado());
            pre.setInt(5, cl.getCodigoCliente());
            
            res = pre.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return res;
    }
    
    public int eliminarCliente(Cliente cl) throws Exception
    {
        int res;
        try 
        {
            this.conectar();
            String sql="update cliente set estado=0 where codigoCliente=?";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setInt(1, cl.getCodigoCliente());
            res = pre.executeUpdate();
            
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return res;
    }
    
}
