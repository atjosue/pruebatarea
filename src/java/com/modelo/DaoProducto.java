package com.modelo;

import com.conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Nombre De La Clase: DaoProducto
 * @author Kevin Antonio Montiel Ramos
 * Fecha: 02/10/2018
 * Version: 1.0
 * Copyrigth: KAMR
 */

public class DaoProducto extends Conexion
{
    
    public List<Producto>mostrarProducto() throws ClassNotFoundException, SQLException, Exception
    {
        List<Producto>mostrarProducto = new ArrayList();
        ResultSet res;
        
        try 
        {
            this.conectar();
            String sql = "select * from producto where estado = 1;";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            res=pre.executeQuery();
            
            while(res.next())
            {
                Producto pro = new Producto();
                pro.setCodigoProducto(res.getInt("codigoProducto"));
                pro.setNombreProducto(res.getString("nombreProducto"));
                pro.setPrecioVenta(res.getDouble("precioVenta"));
                pro.setStockMinimo(res.getInt("stockMinimo"));
                pro.setStockActual(res.getInt("stockActual"));
                pro.setCodigoBarra(res.getString("codigoBarra"));
                pro.setEstado(res.getInt("estado"));
                mostrarProducto.add(pro);
            }
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return mostrarProducto;
    }
    
    public int insertarCliente(Producto pro) throws Exception
    {
        int resuI;   
        try 
        {
            this.conectar();
            String sql = "insert into producto values(?,?,?,?,?,?,?);";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setInt(1, pro.getCodigoProducto());
            pre.setString(2, pro.getNombreProducto());
            pre.setDouble(3, pro.getPrecioVenta());
            pre.setInt(4, pro.getStockMinimo());
            pre.setInt(5, pro.getStockActual());
            pre.setString(6, pro.getCodigoBarra());
            pre.setInt(7, pro.getEstado());
            resuI = pre.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        
        return resuI;
    }
    
    public int modificarProducto(Producto pro) throws Exception
    {
        int resuM;
        
        try 
        {
            this.conectar();
            String sql = "update producto set nombreProducto =?, precioVenta =?, stockMinimo=?, stockActual =?, codigoBarra =? where codigoProducto = ? ;";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setString(1, pro.getNombreProducto());
            pre.setDouble(2, pro.getPrecioVenta());
            pre.setInt(3, pro.getStockMinimo());
            pre.setInt(4, pro.getStockActual());
            pre.setString(5, pro.getCodigoBarra());
            pre.setInt(6, pro.getCodigoProducto());
            
            resuM = pre.executeUpdate();
        } 
        catch (Exception e) 
        {
           throw e;
        }
        finally
        {
             this.desconectar();
        }
        
        return resuM;
    }
    
    public int eliminarProducto(Producto pro) throws Exception
    {
        int resuE;
        
        try 
        {
            this.conectar();
            String sql = "update producto set estado = 0 where codigoProducto = ?;";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setInt(1, pro.getCodigoProducto());
            
            resuE = pre.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        
        return resuE;
    }
    
    
    
    public List<Producto> extraerProducto(Producto p) throws ClassNotFoundException, SQLException, Exception
    {
        List<Producto>listaP= new ArrayList();
        ResultSet res;
        try 
        {
            this.conectar();
            String sql="select * from producto where codigoProducto=?";
            PreparedStatement pre= this.getCon().prepareStatement(sql);
           pre.setInt(1, p.getCodigoProducto());
            res=pre.executeQuery();
            while(res.next())
            {
                Producto pro= new Producto();
                pro.setCodigoProducto(res.getInt("codigoProducto"));
                pro.setNombreProducto(res.getString("nombreProducto"));
                pro.setStockMinimo(res.getInt("stockMinimo"));
                pro.setStockActual(res.getInt("stockActual"));
                pro.setPrecioVenta(res.getDouble("precioVenta"));
                pro.setCodigoBarra(res.getString("codigoBarra"));
                listaP.add(pro);
            }
        }
        catch (ClassNotFoundException | SQLException e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
   
        return listaP;  
    }
 
}

